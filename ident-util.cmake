###############################################################################
# Cmake for cross compilation for AVR
################################################################################

# Set compliler
SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_C_COMPILER avr-gcc)
SET(CMAKE_CXX_COMPILER avr-gcc)

# Set targert device type
SET(MCU "atmega1280")
#SET(MCU "atmega328p")
SET(MCU_SPEED "16000000UL")
# Set programmer options
SET(AVRDUDE_PORT /dev/avr-programmer)
SET(AVRDUDE_OPTIONS -c avrispv2)
#SET(AVRDUDE_PORT /dev/ttyUSB0)
#SET(AVRDUDE_OPTIONS -c arduino -b 57600 -D)

# Set compiler options 
# C compliler flags
SET(CSTANDARD "-std=gnu99")
SET(CDEBUG "-gstabs")
SET(CWARN "-Wall -Wstrict-prototypes")
SET(CTUNING "-funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums")
SET(COPT "-Os")
SET(CMCU "-mmcu=${MCU}")
STRING(TOUPPER ${MCU} MCU_DEFINE)
SET(CDEFS "-DF_CPU=${MCU_SPEED} -D${MCU_DEFINE}")

# Additional CXX flags
SET(CXXTUNING "-fno-threadsafe-statics -fno-exceptions")
SET(CXXSTANDARD "-std=c++11")

SET(CFLAGS   "${CMCU} ${CDEFS} ${CINCS} ${COPT} ${CWARN} ${CDEBUG} ${CSTANDARD} ${CTUNING} ${CEXTRA}")
SET(CXXFLAGS "${CMCU} ${CDEFS} ${CINCS} ${COPT} -Wall ${CDEBUG} ${CTUNING} ${CXXSTANDARD} ${CXXTUNING} ${CXXEXTRA}")

SET(CMAKE_C_FLAGS  ${CFLAGS})
SET(CMAKE_CXX_FLAGS ${CXXFLAGS})

# compiler test
SET(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")

# avrdude programmer flags
SET(AVRDUDE_FLAGS -P ${AVRDUDE_PORT} -p ${MCU} ${AVRDUDE_OPTIONS})

# additional targets to build .hex file and to program device by "building" name.write target
MACRO(AVR_PROGRAM name)
	# Build .hex file if necessary
	ADD_CUSTOM_TARGET(${name}.hex SOURCES ${name}.hex)
	ADD_CUSTOM_COMMAND(OUTPUT ${name}.hex DEPENDS ${name} COMMAND avr-objcopy ARGS -O ihex -R.eeprom ${name} ${name}.hex)

	# Add program target
	ADD_CUSTOM_TARGET(${name}.write COMMENT "Uploading ${name}.hex to target device ..." COMMAND avrdude ARGS ${AVRDUDE_FLAGS} -U flash:w:${name}.hex)
	ADD_DEPENDENCIES(${name}.write ${name}.hex)
ENDMACRO(AVR_PROGRAM)
