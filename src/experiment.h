/** @file experiment.h
 * @brief Experiment module interface.
 * To use firmware with new target device user should implement defined in 
 * this file target specific functions and set constant variables.
 *
 * @see{experiment_template.cpp}
 */
#ifndef IDENT_UTIL_H
#define IDENT_UTIL_H


extern "C" {
#include <avr/io.h>
#include <avr/pgmspace.h>
}

#include "source.hpp"
#include "sink.hpp"
#include "regulator.hpp"
#include "signalbuf.hpp"

extern SignalBuf signal_buf; /**< Signal buffer (state vector). */
extern Sink * sink; /**< Sink object. It reads signal buffer content to output stream. */
extern Source * source; /**< Source object. It writes values from input stream to signal buffer. */
extern Regulator * R; /**< Internal control loop. */


///**
// * @brief Experiment status variable.
// * This variable allows to stop experiment from periodic called function.
// * Any values different from @ref APPLY_STATUS_CONTINUE cause abortion.
// * @see{ApplyStatus, experiment_template.cpp}
// **/
//extern volatile apply_status_t experiment_status;
//
///**
// * @brief Signal buffer lock mutex.
// * It is used to protect periodic task from reentry.
// **/
//extern volatile mutex_t signal_buf_lock;

/**
 * @brief Default signal description string.
 **/
extern const char experiment_signal_none_str[] PROGMEM; 


/**
 * @brief Target specific constant and functions. 
 * Those constant and functions should be inplemented in target specifics file. 
 * See @ref experiment_template.cpp for example.
 * @{
 */ 

/**
 * @brief Short experiment descrition string displayed by @ref dex. 
 * */
extern const char experiment_description[] PROGMEM; 

/**
 * @brief Signal description string array.
 * This array contains description of state vector (signals). State vector size is equal toe @c{8*sizeof(sig_mask_t)}.
 * Description strings have following format:
 *     
 *     <name> <units> <scale> <offset> <min> <max> {cont|sat} {none|sink|source|control}
 *
 * - To convert `raw` value from state vector to physical units <units> use formula @c{value_units = scale*(value_raw - offset)}.
 * - @c{sat|cont} determines what to do if signal value became greater then @c <max> or less then @c <min>. In first mode value saturates, in second mode it is being "wrapped out".
 * - The last field describes signal purpose.
 **/
extern PGM_P const experiment_signal_description[] PROGMEM;

/**
 * @brief Default source signals mask.
 **/
extern const sig_mask_t source_default_mask;
/**
 * @brief Default sink signals mask.
 **/

extern const sig_mask_t sink_default_mask;
/**
 * @brief Default actuator signals mask.
 **/

extern const sig_mask_t control_default_mask;

/**
 * @brief Size of user memory
 **/
 extern const uint8_t experiment_user_mem_size;
 
 extern sig_val_t  experiment_user_mem_ptr[];/**< Pointer to user memory. */
 
/**
 * @brief Experiment initialization hook.
 * This function is called once before parser is started.
 * It performs target device related configuration.
 */
extern void experiment_init(void);

/**
 * @brief Experiment hook.
 * This function is called on command @ref exp.
 * It initialize experiment, call in loop worker functions and finalize it.
 * @return Experiment end status @ref ApplyStatus.
 * @see{experiment_template.cpp}
 */
extern apply_status_t experiment(void);

/**
 * @brief Timer setup hook.
 * Set sample rate and periodic functions using timer interface @ref timer.h .
 */
extern void set_sample_rate(uint16_t period_mcs);

/**
 * @brief Apply values from signal buffer to actuators.
 * Set PWM, DAC output, pin level and i.e.
 */
extern void apply_control(void);

/**
 * @brief Read values form sensors to signal buffer.
 * Read ADC, pin values and so on. 
 */
extern void refresh_sensors(void);

/**
 * @}
 */
#endif
