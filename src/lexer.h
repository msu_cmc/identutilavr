/** @brief Lexical analizer.
 * Split line to @a tokensr. Token is the group of visible symbols separated by space or tabs.
 */
#ifndef  LEXER_H
#define  LEXER_H

/** 
 * @brief Maximal line size
 */
#define LEXER_MAX_LINE_SIZE 50
/** 
 * @brief Maximal token size
 */
#define LEXER_MAX_TOKEN_SIZE 20

/** 
 * @brief Get next token.
 * Return the next token or null string if there is no more tokens in current line. 
 * @param token Pointer to char array. The size of the array should be long enough.
 **/
extern void lexer_get_token(char token[]);

/**
 * @brief Switch current line to the next line.
 */
extern void lexer_newline(void);

#endif  /*LEXER_H*/
