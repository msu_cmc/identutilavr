#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "adc.h"

void adc_init_single(void)
{
	ADC_ADMUX = 0;
	ADC_ADCSRA = 0;
	// analog reference -- AVCC 
	ADC_ADMUX |= _BV(REFS0);
	// conversation result is right aligned
	ADC_ADMUX &= ~_BV(ADLAR);
	// Select input: ch1 ADC0, ch2 ADC3 
	// Select input: single channel, ADC0 (PF0, A0 pin)
	ADC_DDR1 &= ~_BV(ADC_CH1); // input
	ADC_PORT1 &= ~_BV(ADC_CH1); // pull up disabled
	ADC_DIDR1 |= _BV(ADC_DID1); // disable input logic
	// Select input: single channel, ADC3 (PF3, A3 pin).
	ADC_DDR2 &= ~_BV(ADC_CH2); // input
	ADC_PORT2 &= ~_BV(ADC_CH2); // pull up disabled
	ADC_DIDR2 |= _BV(ADC_DID2); // disable input logic
	// Prescaler division factor = 128, ADC_CYCLE_MCS = 128/16 = 8
	ADC_ADCSRA |= _BV(ADC_ADPS0) | _BV(ADC_ADPS1) | _BV(ADC_ADPS2); 
	// ADC is enabled
	ADC_ADCSRA |= _BV(ADC_ADEN);
	// Interrupt is disabled.
	// Free running mode disabled.
}

void adc_deinit(void)
{
	// disable ADC module
	ADC_ADCSRA &= ~_BV(ADC_ADEN);
	// enable logic input on ADC0
	ADC_DIDR1 &= ~_BV(ADC_DID1); // enable input logic ch 1
	ADC_DIDR2 &= ~_BV(ADC_DID2); // enable input logic ch 2
}
