/**
 * @file rtcommon.h
 * Common RTSerial configuration.
 **/
#ifndef  RTCOMMON_H
#define  RTCOMMON_H

#include "config.h"

#define STARTBYTE 'A' /**< Frame start byte. */
#define TAILBYTE 'B' /**< Frame tail byte. */

/**
 * @define RT_BAUDRATE
 * @brief RTSerial UART baudrate.
 */
#if !defined(RT_BAUDRATE)
#define RT_BAUDRATE 115200
#endif /* RT_BAUDRATE */

/**
 * @define RT_CONFIGURE_UART
 * @brief If set *_init() functions performs UART configuration.
 */
#if defined(RT_CONFIGURE_UART)
#	define RT_CONFIGURE_UART 0
#endif 

/**
 * Hardware dependent defines for AVR MCU.
 * @{
 */
#if !defined (RT_UART)
#	define RT_UART 0
#	define RT_RX_SIG USART0_RX_vect
#	define RT_TX_SIG USART0_UDRE_vect
#	define RT_UDR UDR0
#	define RT_UCSRA UCSR0A
#	define RT_UCSRB UCSR0B
#	define RT_UCSRC UCSR0C
#	define RT_MPCM MPCM0
#	define RT_RXC RXC0
#	define RT_TXC TXC0
#	define RT_UDRE UDRE0
#	define RT_FE FE0
#	define RT_DOR DOR0 
#	define RT_PE UPE0
#	define RT_RXCIE RXCIE0
#	define RT_TXCIE TXCIE0
#	define RT_UDRIE UDRIE0
#	define RT_RXEN RXEN0
#	define RT_TXEN TXEN0
#	define RT_UCSZ0 UCSZ00
#	define RT_UCSZ1 UCSZ01
#	define RT_UCSZ2 UCSZ02
#	define RT_PM0 UPM00
#	define RT_PM1 UPM01
#	define RT_USBS USBS0
#	define RT_UMSEL0 UMSEL00
#	define RT_UMSEL1 UMSEL01
#	define RT_UCPOL UCPOL0
#	define RT_UBRR UBRR0
#endif
/**
 * @}
 */

#endif  /*RTCOMMON_H*/
