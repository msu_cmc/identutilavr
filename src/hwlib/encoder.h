/** @file encoder.h
 * @brief Encoder interface.
 */
#ifndef ENCODER_H
#define ENCODER_H

extern int16_t encoder_position;
extern int8_t encoder_dir;

extern void encoder_init(void);

#endif

