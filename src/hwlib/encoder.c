#include <avr/io.h>
#include <avr/interrupt.h>
 
#include "config.h"
#include "encoder.h"

/**
@addtogroup hardware
@{
#### Relative encoder connections ##########

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PD2/INT2| 19				| input, no pull-up	| A channel
PD3		| 18				| input, no pull-up	| B channel
@}
*/

/**
@defgroup encoder Encoder module
@{
Encoder interface. Encoder position changes on falling and rising edges of encoder A cannel (double resolution).
*/

int16_t encoder_position; ///< Current encoder position, shared resource, must be accessed only from critical sections.

/**
 * Init encoder.
 * Configure inputs.
 * Enables external interrupt INT2 on falling and rising channels.
 **/ 
void encoder_init(void) 
{
	encoder_position = 0;
	// Setup PD3 and PD2/INT2 configured as inputs, pull-up disabled.
	ENCODER_DDR &= ~(_BV(ENCODER_CHA) | _BV(ENCODER_CHB));
	ENCODER_PORT &= ~(_BV(ENCODER_CHA) | _BV(ENCODER_CHB));
	// Setup INT2
	ENCODER_SIG_MSKR &= ~_BV(ENCODER_SIG_IE); // Disable INT2.  
	ENCODER_SIG_ICR |= _BV(ENCODER_SIG_ISC0); EICRA &= ~_BV(ENCODER_SIG_ISC1); //Rising and falling edges on A channel generate INT
	ENCODER_SIG_IFR = _BV(ENCODER_SIG_IE); // Clear INT flag.
	ENCODER_SIG_MSKR |= _BV(ENCODER_SIG_IE); // Enable INT2.
}

/** 
 * Encoder interrupt handler.
 * PD2/INT2(19) --- A channel, PD3(18) --- B channel. 
 */
ISR(ENCODER_SIG_CHA_INT) 
{
	//INT2've occured, INTs are disabled.
	const uint8_t CHAB = _BV(ENCODER_CHA)|_BV(ENCODER_CHB);

	if ( (ENCODER_PIN & CHAB) == CHAB || (~ENCODER_PIN & CHAB) == CHAB ) {
		encoder_position--;
	}
	else {
		encoder_position++;
	}	
#undef CHAB
}
/**
@}
*/
