/** @file pwm2.h
@brief Control interface for two pulse-width modulators.
*/

#ifndef PWM_H_2
#define PWM_H_2

#define PWM_TOP 0x3ff

extern void motor_init_2(void);

extern void motor_in_pwm1(int16_t pwm);
extern void motor_in_pwm2(int16_t pwm);

#endif

