#include <avr/io.h>
#include <stdio.h>

#include "config.h"
#include "uart0.h"

static int uart0_putchar(char c, FILE *stream);
static int uart0_getchar(FILE *stream);

FILE uart0 = FDEV_SETUP_STREAM(uart0_putchar, uart0_getchar, _FDEV_SETUP_RW);
int8_t uart0_rx_overun = 0;

static int uart0_putchar(char c, FILE *stream) 
{
//  if (c == '\n') uart0_putchar('\r', stream);
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = c;
  return 0;
}

static int uart0_getchar(FILE *stream) 
{
	__attribute__((unused)) int c;
	loop_until_bit_is_set(UCSR0A, RXC0);
	// detect frame error, parity error or buffer overrun error
	uart0_rx_overun |= UCSR0A & _BV(DOR0);
	if (UCSR0A & (_BV(FE0)|_BV(UPE0)|_BV(DOR0))) {
		c = UDR0;
		return _FDEV_ERR;
	} 
	else return UDR0;
}

void uart0_init(void)
{
	// Set baurdrate
#define BAUD UART0_BAUDRATE
#include <util/setbaud.h>
	UBRR0L = UBRRL_VALUE;
	UBRR0H = UBRRH_VALUE;
	// Set mode 
	UCSR0A = 0;
#ifdef USE_2X
	UCSR0A |= _BV(U2X0);
#endif
	// TX enabled, RX enbled, interrupts disabled, 8 data bits, 1 stop bits, no parity
	UCSR0B = _BV(TXEN0) | _BV(RXEN0);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

void uart0_deinit(void)
{
	// TX disable, RX disable, interrupts disabled
	UCSR0B = 0;
	UCSR0C = 0;
	UCSR0A = 0;
	// Reset TC
	UCSR0A |= _BV(TXC0);
}
