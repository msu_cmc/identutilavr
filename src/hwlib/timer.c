#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer.h"

#if defined(TIMER_16BIT)
#	include "timer16bit.c"
#endif /* TIMER_16BIT*/

#if defined(TIMER_8BIT)
#	include "timer8bit.c"
#endif /* TIMER_8BIT*/
