/** @file adc.h
 * @brief ADC interface.
 */
#ifndef ADC_H
#define ADC_H

#include "config.h"

#define ADC_CYCLE_MCS 8

/**
@defgroup adc ADC module
@{
Result of analog-to-digital conversation is 10-bit integer. Zero coresponds to zero voltage. 1023 coresponds to maximal voltage (VCC).
See @ref hardware for more information on hardware connections.
*/

/**
 * Init ADC module. 
 * Setup ADC with division factor 128. On speed 16MHz it corresponds to 8 mcs on ADC cycle and convertion takes 8*14=112mcs.
 * Reference signal is VCC, this is resonable choose for potentiometer position sensors.
 * Disable input logic on ADC inputs.
 * ADC interrupt is disabled. 
 * */ 
void adc_init_single(void);


/** Deinit ADC module.
 * Disable ADC unit, enables input logic on ADC inputs.
 */
void adc_deinit(void);

/** 
 * Select first ADC input channel ADC0. 
 **/
inline void adc_select_ch1(void)
{
	ADC_ADMUX = ADC_ADMUX1_VALUE;
	ADC_ADCSRB = (ADC_ADCSRB & ~ADC_ADCSRB1_MSK) | _BV(ADC_ADCSRB1_VALUE);
}

/** 
 * @fn inline void adc_select_ch2(void)
 * Select second ADC input channel ADC3.
 **/
inline void adc_select_ch2(void)
{
	ADC_ADMUX = ADC_ADMUX2_VALUE;
	ADC_ADCSRB = (ADC_ADCSRB & ~ADC_ADCSRB2_MSK) | _BV(ADC_ADCSRB2_VALUE);
}

/** 
 * @fn inline void adc_single_start(void)
 * Start analog-to-digital conversation.
 **/
inline void adc_single_start(void)
{
	// Start conversation
	ADC_ADCSRA |= _BV(ADC_ADSC);
}

/**
 * Check if single conversation is finished.
 * @return true, if conversation has finished
 */  
inline int16_t adc_single_check(void)
{
	return ADC_ADCSRA & ADC_ADIF;
}

/**
 * Wait for the end of conversation and return conversation result.
 * @return conversation result
 */  
inline int16_t adc_single_wait_get(void)
{
	loop_until_bit_is_set(ADC_ADCSRA, ADC_ADIF);
	ADC_ADCSRA |= ADC_ADIF; // disable flag
	return ADC_ADC;
}

/**
 * Get result of last conversation. 
 * @return conversation result
 */  
inline int16_t adc_single_get(void)
{
	return ADC_ADC;
}

/**
@}
**/

#endif

