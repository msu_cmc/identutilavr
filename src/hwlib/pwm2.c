#include <avr/io.h>

#include "config.h"
#include "pwm2.h"

/**
@defgroup pwm PWM_2 output module
@{
*/

/**
 * @define PWM_TOP
 * @brief PWM timer TOP value
 */

/** 
 * Init motor control module. 
 * Must be called before any other motor functions. Setup Fast 10-bits PWM mode
 */

void motor_init_2(void) {
    
    DDRC |= _BV(PC0) | _BV(PC1);//set as out for 1 and 2 en
    
    DDRC |= _BV(PC2) | _BV(PC3);//in1 as out for 1 and 2 in1
    
    DDRC |= _BV(PC4) | _BV(PC5);//set as out for 1 and 2 ds2
    
    DDRB |= _BV(PB5) | _BV(PB6);//set as out for 1 and 2 in2
    
    //set pwm=0
    OCR1A = 0x0000;
    OCR1B = 0x0000;
    
    //set fast 10-bits pwm mode
    TCCR1A &= ~(_BV(WGM13));
    TCCR1A |= _BV(WGM12) | _BV(WGM11) | _BV(WGM10);
    
    //set non-inverting mode for 1 and 2 pwm
    TCCR1A &= ~(_BV(COM1A0) | _BV(COM1B0));
    TCCR1A |= _BV(COM1A1) | _BV(COM1B1);

    //turn on H-bridge
    //set en as 1    
    PORTC |= _BV(PC0);
    PORTC |= _BV(PC1);
    
    //set ds2 as 0
    PORTC |= _BV(PC4);//ds2
    PORTC |= _BV(PC5);
    
    //turn on timer. Tt1 = T0clk/1
	// Effective frequency 16 kHz 
    TCCR1B &= ~(_BV(CS12) | _BV(CS11));
    TCCR1B |= _BV(CS10);
    
}

void motor_in_pwm1(int16_t pwm) 
{
	if (pwm >= 0) {
		// (IN1, IN2) = (LOW, HIGH) 
		PORTC &= ~_BV(PC2);
		// non-inverting PWM mode
		TCCR1A &= ~_BV(COM1A0);
		TCCR1A |= _BV(COM1A1);
		
		OCR1A = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
	} else {
		pwm = -pwm; 
		// (IN1, IN2) = (HIGH, LOW) 
		PORTC |= _BV(PC2);
		// inverting PWM mode
		TCCR1A |= _BV(COM1A0);
        TCCR1A |= _BV(COM1A1);

		OCR1A = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
	}
}

void motor_in_pwm2(int16_t pwm) 
{
	if (pwm >= 0) {
		// (IN1, IN2) = (LOW, HIGH) 
		PORTC &= ~_BV(PC3);
		// non-inverting PWM mode
		TCCR1A &= ~_BV(COM1B0);
		TCCR1A |= _BV(COM1B1);
		
		OCR1B = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
	} else {
		pwm = -pwm; 
		// (IN1, IN2) = (HIGH, LOW) 
		PORTC |= _BV(PC3);
		// inverting PWM mode
		TCCR1A |= _BV(COM1B0);
        TCCR1A |= _BV(COM1B1);

		OCR1B = (pwm <= PWM_TOP) ? pwm : PWM_TOP;
		
	}
}

/**
@}
*/
