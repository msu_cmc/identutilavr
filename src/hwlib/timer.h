/** @file timer.h
 * @brief Timer interface.
 *
 * Interface for 8bit or 16bit timer of target device.
 */
#ifndef  TIMER_H
#define  TIMER_H

#include "config.h"

extern void timer_init(void);

extern void timer_set_period(uint16_t period_mcs, void (* callback)(void));

extern void timer_set_oc1(uint16_t ocr_value_mcs, void (* callback)(void));

extern void timer_set_oc2(uint16_t ocr_value_mcs, void (* callback)(void));

extern void timer_reset(void);

inline void timer_start(void) 
{
	// prescaler Tclk/8, clock start, 2 clk ticks == 1 mcs
	//TODO adapt for arbitrary clock rate
	TIMER_TCCRB |= _BV(TIMER_CS1);
}

inline void timer_stop(void) 
{
	// stop clock
	TIMER_TCCRB &= ~(_BV(TIMER_CS0) | _BV(TIMER_CS1) | _BV(TIMER_CS2));
}

#endif  /*TIMER_H*/
