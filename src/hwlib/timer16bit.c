#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer.h"

/**
@defgroup timer Timer module
@{
@brief Timer interface. 

Timer interface allows to use 8bit and 16bit timer to run tasks in fixed periods of time.

Example. Run @c task1() and @c task2() each 10 ms. 
@c task1() will be called in 3 ms, 13 ms, 23 ms and so on after timer start.
@c task2() will be called in 10 ms, 20 ms, 30 ms after timer start. 


    void task1(void) {
    	...
    }
    
    void task2(void) {
    	...
    }
    
    void main() {
    	timer_init();
    	timer_set_period(10000, task2); 
    	timer_set_oc1(3000, task1); 
    	timer_start();
    	...
    	timer_stop();
    }
    

*/


static struct {
	void (*callback)(void);
	void (*oc1_callback)(void);
	void (*oc2_callback)(void);
} timer;

/**
 * Timer initializations.
 *
 * CTC mode, Tclk = clk/8, intrrupts are disabled.
 */
void timer_init(void) 
{
	// CTC mode, TOP in OCR3A
	TIMER_TCCRA = 0;
	TIMER_TCCRB = 0;
	TIMER_TCTCR = _BV(TIMER_CTC);
	//disable interrupts
	TIMER_TIFR = _BV(TIMER_OC1IE) | _BV(TIMER_OC2IE) | _BV(TIMER_TOIE);
	TIMER_TIMSK &= ~(_BV(TIMER_OC1IE) | _BV(TIMER_OC2IE) | _BV(TIMER_TOIE));
	//clear callbacks
	timer.callback = 0;
	timer.oc1_callback = 0;
	timer.oc2_callback = 0;
}

/**
 * Set timer period in mcs.
 *
 * @param period_mcs New timer period in mcs.
 * @param callback Callback on timer overflow event. Disable TOV interrupt if callback is NULL.
 **/
void timer_set_period(uint16_t period_mcs, void (* callback)(void))
{
	TIMER_TTOP = (period_mcs<<1) - 1;
	timer.callback = callback;
	if (callback) {
		TIMER_TIFR = _BV(TIMER_TOIE);
		TIMER_TIMSK |= _BV(TIMER_TOIE);
	}
	else {
		TIMER_TIMSK &= ~_BV(TIMER_TOIE);
	}
}

/**
 * Set output compare match 1.
 *
 * @param oc_mcs Time interval between start of timer period and OC interrupt.
 * @param callback OC event handler. If callback is equal to 0, disable interrupt. 
 */
void timer_set_oc1(uint16_t ocr_value_mcs, void (* callback)(void)) 
{
	TIMER_OCR1 = (ocr_value_mcs<<1);
	timer.oc1_callback = callback;
	if (callback) {
		TIMER_TIFR = _BV(TIMER_OC1IE);
		TIMER_TIMSK |= _BV(TIMER_OC1IE);
	}
	else {
		TIMER_TIMSK &= ~_BV(TIMER_OC1IE);
	}
}

/**
 * Set output compare match 2.
 *
 * @param oc_mcs Time interval between start of timer period and OC interrupt.
 * @param callback OC event handler. If callback is equal to 0, disable interrupt. 
 */
void timer_set_oc2(uint16_t ocr_value_mcs, void (* callback)(void))
{
	TIMER_OCR2 = (ocr_value_mcs<<1);
	timer.oc2_callback = callback;
	if (callback) {
		TIMER_TIFR = _BV(TIMER_OC2IE);
		TIMER_TIMSK |= _BV(TIMER_OC2IE);
	}
	else {
		TIMER_TIMSK &= ~_BV(TIMER_OC2IE);
	}
}

/**
 * @fn inline void timer_start(void)
 * Start timer clock.
 */

/**
 * @fn inline void timer_stop(void)
 * Stop timer clock.
 */

/**
 * Set timer counter to 0.
 */
void timer_reset(void)
{
	TIMER_TCNT = 0;
}

/**
@}
**/

ISR(TIMER_SIG_TOV) 
{
	(*timer.callback)();
}

ISR(TIMER_SIG_OC1) 
{
	(*timer.oc1_callback)();
}

ISR(TIMER_SIG_OC2) 
{
	(*timer.oc2_callback)();
}

