#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer.h"

static struct {
	uint8_t cnt;
	uint8_t top;
	void (*callback)(void);
	uint8_t ocr1;
	void (*oc1_callback)(void);
	uint8_t ocr2;
	void (*oc2_callback)(void);
} timer;

void timer_init(void) 
{
	// CTC mode, TOP in OCRA
	TIMER_TCCRA = 0;
	TIMER_TCCRB = 0;
	TIMER_TCTCR = _BV(TIMER_CTC);
	//setup timer
	TIMER_TCNT = 0;
	timer.cnt = 0;
	//TODO arbitrary clock rate
	//set TOP value. TOV interrupt occurs every 100 mcs.
	TIMER_TTOP = 199;
	//enable interrupts
	TIMER_TIFR = _BV(TIMER_TOIE);
	TIMER_TIMSK |= _BV(TIMER_TOIE);
	//clear callbacks
	timer.callback = 0;
	timer.oc1_callback = 0;
	timer.oc2_callback = 0;
}

void timer_set_period(uint16_t period_mcs, void (* callback)(void))
{
	timer.top = period_mcs/100 - 1;
	timer.callback = callback;
}

void timer_set_oc1(uint16_t ocr_value_mcs, void (* callback)(void)) 
{
	timer.ocr1 = ocr_value_mcs/100;
	timer.oc1_callback = callback;
}

void timer_set_oc2(uint16_t ocr_value_mcs, void (* callback)(void))
{

	timer.ocr2 = ocr_value_mcs/100;
	timer.oc2_callback = callback;
}

void timer_reset(void)
{
	TIMER_TCNT = 0;
	timer.cnt = 0;
}

ISR(TIMER_SIG_TOV, ISR_NOBLOCK) 
{
	if (timer.cnt == timer.top) {
		timer.cnt = 0;
		return;
	}
	timer.cnt++;

	if (timer.callback && timer.cnt == timer.top) (*timer.callback)();
	if (timer.oc1_callback && timer.cnt == timer.ocr1) (*timer.oc1_callback)();
	if (timer.oc2_callback && timer.cnt == timer.ocr2) (*timer.oc2_callback)();
}
