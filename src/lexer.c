#ifndef  LEXICAL_ANALIZER_H
#define  LEXICAL_ANALIZER_H

#include <ctype.h>
#include <stdio.h>

#include "lexer.h"
#include "bool.h"

static char line[LEXER_MAX_LINE_SIZE];
volatile static uint8_t index;
volatile static char c;

inline static void gc(void) 
{
	c = line[index];
	if (c != 0) index++;
}

#define isnl(c) (((c) == '\n') ? TRUE : FALSE)

void lexer_get_token(char token[])
{
	uint8_t i;

	while (isspace(c) && !isnl(c)) {
		gc();
	} 

	i = 0;
	for(; c != 0 && !isspace(c); gc()) {
		if (i < LEXER_MAX_TOKEN_SIZE-1) {
			token[i] = c;
			i++;
		}
	}
	token[i] = 0;
}

void lexer_newline(void)
{
	gets(line);
	index = 0;
	gc();
}

#endif  /*LEXICAL_ANALIZER_H*/
