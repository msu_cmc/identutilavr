/**
@file description.h 
@brief File with Doxygen documentation.

@mainpage AVR IdentControl Tool Documentation

Overview
--------

AVR IdentControl Tool is firmware for Arduino based boards (or any AVR based board), 
to perform experiments with attached to board sensors and actuator devices.
Together with Octave IdentControl package it forms a very rough equavalent to MatLab Arduino
framework.

Utility is meant to be used as demostration tool for different Control Theory concepts, 
and to perform experiments with real plants (cheap robotics).  
You can send to plant connected to board arbitrary signal (usually PWM modulated)
and recive plant responce. 

IdentControl Tool can be used as a bridge between hardware and OROCOS control system running on PC (see @ref irt, @ref ort commands).

Additionaly you can add to simple control loop. Curently supported 
regulators includes:
- digital PID,
- relay,
- low order transfer function.

Curently two targets devices are implemented.

- @b experiment_servo. 
  Perform experiments with servo and potentiometer encoder as setting device.
  ADC channel 1 and 2 are used to measure servo position and desired setpoint. One PWM cahnnel is used to drive servo motor.

- @b experiment_motor. 
  Perform experiment with DC motor with relative encoder for position feedback. 
  (Position is set to zero before each experiment). 

Content
-------

@ref building

@ref hardware 

@ref protocol

@defgroup building Building and Installing firmware
@{

Project uses cmake build system. 

To build and upload firmare perform following steps: 

-# Edit ident-util.cmake. Set type of AVR programmer, programmer port.
-# Configure and build project:
@code
    cmake .
    make
@endcode
-# To build this documentation (egg and chicken problem?):
@code
    make doc
@endcode
-# Program device 
@code
    make <target>.write
@endcode
@}

@defgroup hardware Hardware Reqvirements and Connections
@{

Atmega1280 device with 16MHz clock. 
You may use Arduino Mega with any H-brige.

### Serial communications ###

Use USART0 of the board for serial communications. 
Serial line should be configured to \b 57600 \b Baurd, \b 1 \b stop, \b 8 \b data, \b no \b parity mode.


@}

@defgroup protocol Communication protocol 
@{

Communication protocol allows to control board from serial RS232 link.

Serial line should be configured to \b 57600 \b Baurd, \b 2 \b stop, \b 8 \b data, \b no \b parity mode.

Device is working in slave mode.  It recives ASCII commands from computer.
Each command consists of three letters and, if necessary, parameters, separated by whitespace. 
Line ends by newline symbol.

Device response depends on recived command.
In general case it contains several lines which may include raw data. 
Device response ends with line starting from "OK" or "Failed".
In first case previous command was complited successfully. 
Response line starting from "Failed" indicates error.
See @ref error for more information on error codes. 

There are commands, which configure internal parameters (for example, \c srt, \c ibs) of firmware and comands, 
which behaviour depends on those parameters. For example, before perfoming experiment (\c exp command)
you should set sample rate (\c srt), configure output (\c ops, \c obs) and input (\c ibs, \c ibp).

Communication  concept
----------------------

The device has a state vector (also called signal buffer). This vector stores current reference value, sensor's output and control.
Command starting from 'i' create @b source objects, which write values to state vector from standart input stream or from other source.
Command starting from 'o' create @b sink objects, which read variable values from state vector to output stream or to other destination.
Command begining from 'r' create @b regulator objects, which change values of state vector varibales 
according to predefined control law. The @b mask parameter determines the set of state varibales. Given object 
(sink, source or regulator) interact with variable if cooresponding bit in mask is set.

Only one object of each type exists in firmware memory.

For example, assume the state vector consists of tree variables: position, pwm and reference_position.
So sink object with mask 0x03 (0b00000101) prints position and reference_position values.  
Source with mask 0x4 (0b00000100) sets reference_position variables value.
And regulator with  ref_mask = 0x4, in_mask = 0x1 and out_mask = 0x2 calculate pwm value from tracking error 
beetween reference position and position. Usually sane default mask values defined (see @ref dex).


Use @ref dex and @ref dsg commands to get signal description. 

Serial protocol example
-----------------------
Symbol in the begin of a line indicates user input (`>`) or device response (`<`).
This command sequence setups and performs a step response experiment with input amplitude equals to 500.
The state vector consists of two variables: @a plant_input (@c{mask=0x1}) and @a plant_output (@c{mask=0x2})

    > srt 10000
    < OK
    > ibs 1 1 0x1
    < Start load
    < decimal 1 1
    > 500
    < End load
    < OK
    > obs 100 0 1 0x2
    < OK
    > exp
    < Start exp
    < decimal 100 1
    < ...
    < End exp
    < OK
	
@section protocol_desc Communication protocol
                                             
- @ref srt                                   
- @ref mod                                   
                                             
Description                                  
- @ref dex                                   
- @ref dsv                                   
                                             
Setup input                                  
- @ref ibs                                   
- @ref ibp                                   
- @ref irt                                   
- @ref ids                                   
                                             
Setup output                                 
- @ref ops                                   
- @ref obs                                   
- @ref ort                                   
- @ref ods                                   
                                             
Regulators and compensators                  
- @ref rno                                   
- @ref rol                                   
- @ref rpi                                   
- @ref rtf                                   
- @ref rrl                                   
- @ref rcc
- @ref rci
                                             
Perform experiment
- @ref exp

Misc
- @ref ver
- @ref set
- @ref get

@subsection srt	`srt` command (set Sample RaTe) 

#### Command format: 

    srt sample_period

#### Response format:

    OK

#### Description: 

Set sample period to `sample_period` mcs (on 16 MHz speed device). 

@subsection mod `mod` command (input-output MODe)  

#### Command format: 

    mod <mode> 

#### Response format:

    OK

#### Description: 

Select I/O mode. If \a mode is "d", then select `decimal` mode. If \a mode is "r" then select `raw` mode.
For more information see @ref ibs , @ref ops and @ref exp.

@subsection dex `dex` command (Describe EXperiment)

#### Command format: ####

    dex

#### Response format: #####

	signals: <n_signals> <sink_default_mask> <control_default_mask> <sink_default_mask> 
    <experiment description>
    OK

#### Description: ##### 

Print brief experiment description. @a n_signals is state vector length.
@a ref_default_mask, @a in_default_mask and @a out_default_mask are default 
source, control and sink masks.

The next line contains brief experiment description.

@subsection dsv `dsv` command (Describe Signal Vector)

#### Command format: ####

    dsv <variable_number>

#### Response format: #####

	
    <variable_name> <unit> <conv_coeff> <conv_offset> <min_val> <max_val> <type> <role>
    OK

#### Description: ##### 

Prints state variable description: the name of variable @a variable_name, variable measuring units @a unit 
(Ampers, Volts and so on), conversation coefficient and offset:

    var_value_in_units = conv_coeff*(var_value - offset)

@a min_val and @a max_val are minimal and maximal value of variable, @a type can be 'sat' or 'cont'.
In latter case the value of variable change in time countinously, but value of variable jumps due to
integer overflow. The @a role can be 'sink', 'control' ,'source' or 'none'.

@subsection ibs `ibs` command (Input Buffered Single)

#### Command format: ####

    ibs <n_samples> <input_div> [<source_mask>]

#### Response format: #####

    Start load
    <io_mode> <n_samples> <sample_dim>
    End load
    OK

#### Description: ##### 

Setup source object.

Store input data in memory buffer. @a n_samples is the number of samples. 
Parameter @a input_div is divider for sample rate. If @a input_div is equal 1, new
sample is extracted from buffer on each control cycle, if @a input_div is 2.
new sample is extracted on each second cycle and so on.
@a source_mask is optional mask parameter. Sample dimention @a sample_dim is equal to
number of set bits in mask. 

Line `"Start load"` indicates, that the device is ready to input values. In `"decimal"` 
input mode  @a n_samples * @a sample_dim decimal numbers are expected. Each group of 
@a sample_dim integers are treated as one sample. During experiment samples are
cpopied to state vector according to @a source_mask.

In `"raw"` mode input bytes are decoded a sequence
little-endian 16-bit signed integer numbers. When device gets enought samples line it
prints lines `"End load"` and `"OK"`. 

@subsection ibp `ibp` command (Input Buffered Periodic)  

#### Command format: 

    ibp <n_sapmles> <n_periods> <input_div> [<source_mask>]

#### Response format:

    Start load
    End load
    OK

#### Description: 

Setup source for following experiments.

Same as @ref ibs , but repeat content of input buffer \a n_periods times.

@subsection irt `irt` command (Input Real Time)  

#### Command format: 

    irt <input_div> <timeout> [<source_mask>]

#### Response format:

    OK

#### Description: 

Read frames with packed input signal from RT_UART serial port using RTSerial protocol.
If no data is received for \a timeout control cycles return to command mode.
Frame format (in bytes):

0          | 1            | 2        | 3         |     | 2N       | 2N+1      | 2N+2
-----------|--------------|----------|-----------|-----|----------|-----------|----------
start_byte | frame_number | data1low | data1high | ... | dataNlow | dataNhigh | stop_byte 

@see{ort, rtrecv.h}

@subsection ids `ids` command (Input DeScribe)  

#### Command format: 

    ids

#### Response format:

    <command> <arg1> .. <argN>
    OK

#### Description: 

Print last successful source command.

@subsection ops `ops` command (Output Print Signal)  

#### Command format: 

    ops <n_samples> <delay> <output_div> [<sink_mask>]

#### Response format:

    OK

#### Description: 

Setup output for following experiments. 

Skip \a delay control cycles then on each \a output_div control cycle output samples will be transfered to the host throught serial link. 
More presicely the output sample will be printed if following expression is `true`.

    ((cycle_number - delay) % output_div) == 0

Print operation in `decimal` mode takes apprtoximately 1.2 ms on baurdrate 57600. In `raw` mode it takes `0.4` ms.

For more details on output format see @ref exp .

@subsection obs `obs` command (Output Buffered Signal)

#### Command format: 

    obs <n_samples> <delay> <output_div> [<sink_mask>]

#### Response format:

    OK

#### Description: 

Setup output during for experiments.

Same as @ref ops , but does not printed samples during experiment. 
Samples are stored in buffer and then sended to the host when experiment has ended.

@subsection ort `ort` command (Output Real Time)  

#### Command format: 

    ort <input_div> [<sink_mask>]

#### Response format:

    OK

#### Description: 

Write frames with packed output signal to RT_UART serial port using RTSerial protocol.
Frame format (in bytes):
    
0          | 1            | 2        | 3         |     | 2N       | 2N+1      | 2N+2
-----------|--------------|----------|-----------|-----|----------|-----------|----------
start_byte | frame_number | data1low | data1high | ... | dataNlow | dataNhigh | stop_byte 

If @ref irt is not running @c frame_number is equal to 0. Otherwise it is equal to @c frame_number
of the last received frame.

@see{irt, rtsend.h}

@subsection ods `ods` command (Output DeScribe)  

#### Command format: 

    ods

#### Response format:

    <command> <arg1> .. <argN>
    OK

#### Description: 

Print the last successful sink creation command.

@subsection rno `rno` command (Regulator Open Loop)  

#### Command format: 

    rno

#### Response format:

    OK

#### Description: 

Do nothing regulator.

@subsection rol `rol` command (Regulator Open Loop)  

#### Command format: 

    rol [<source_mask>] [<control_mask>]

#### Response format:

    OK

#### Description: 

Openloop regulator: copy state variable defined by @a source_mask to @a control_mask.

@subsection rpi `rpi` command (Regulator PID)  

#### Command format: 

    rpi <p> <i> <d> [<source_mask> [<sink_mask> [<control_mask>]]]

#### Response format:

    OK

#### Description: 

Close control loop with digital PID regulator. 
@a source_mask, @a sink_mask and @a control_mask define referece variable, plant output and plant input.
\a p , \a d , \a i are the parameters of regulator as signed fixed point 10.6-bit, 12.4-bits and 8.8-bit 
numbers in decimal representation.
For example, fixed 8.8 bit: `1.25 = 1 + 64/256`, its decimal form is `1*256 + 64 = 320`.

@subsection rtf `rtf` command (Regulator Transfer Function)  

#### Command format: 

    rpi <m> <n> [<source_mask> [<sink_mask> [<control_mask>]]]

#### Response format:

    Start load
    <io_mode> 2 <max(n,m+1)>
    b0 b1 ... b_m 0 ... 0
    a1 a2 ... a_n 0 ... 0
    End load
    OK

#### Description: 

Close control loop with digital transfer function. 
@a source_mask, @a sink_mask and @a control_mask define referece variable, plant output and plant input.
Parameters @a m and @a n are degrees of numerator and denumerator correspondingly.
The transfer function has the form
@f[
    R(z) = /frac{b_0 + b_1 z^{-1} + \dots + b_m z^{-m}}{1 + a_1 z^{-1} + \dots + a_n z^{-n}}.
@f]
Device loads transfer function coefficients as matrix with two rows (see @ref ibs).
Each element of the matrix is 8.8 fixed point number in decimal representation.
Rows are padded with zeros if it is necessary.   

@subsection rrl `rrl` command (Regulator ReLay)  

#### Command format: 

    rrl <A> [<source_mask> [<sink_mask> [<control_mask>]]]

#### Response format:

    OK

#### Description: 

Close control loop with relay regulator:
@f[
    u = A {\mathop sign} (g - y).
@f]
where \a g is input signal, \a y is output of the plant. 

@subsection rcc `rcc` command (Regulator Compensator Coulomb)  

#### Command format: 

    rcc <A_coulomb> 

#### Response format:

    OK

#### Description: 

Compensate dry friction. 
@f[
    u = \left\{
    \begin{array}{ll}
    g + A_{coulomb}{\mathop sign \,} \dot y, & \mbox{if } \dot y \ne 0,\\
    g + A_{coulomb}{\mathop sign \,} g, & \mbox{if }\dot y = 0,	
    \end{array}
    \right.
@f]
where \a g is input signal, \a y is output of the plant, 

@subsection rci	`rci` command (Regulator Compensator Insensevity)  

#### Command format: 

    rci <A_insens> 

#### Response format:

    OK

#### Description: 

Compensate insensetivity zone with \a A_insens amplitude. 

@subsection exp `exp` command (EXPeriment)  

#### Command format: 

    exp 

#### Response format:

    Start exp
    <mode> <n_samples> <sample_dim>
    <sample_1>
    ...
    <sample_n>
    End exp
    OK

#### Description: 

Perform configured experiment. Line `"Start exp"` signifies the beginig of experiment. 
Tag \a mode can be `decimal` or `int16x86`. The first tag indicate `decimal` I/O mode,
the second tag indicates `raw` mode. \a n_samples is the number of samples and \a sample_dim is
the dimension of single sample.

If output is multidimensional, then the values from the sample will be printed consequently. 
In `decimal` mode each line contains one sample and decimal numbers in sample is separated by space. 
In `raw` mode the first \a sample_dim 16-bit words form first sample, following \a sample_dim 
16-bit words forms second sample and so on.

@subsection ver `ver` command (VERsion)

#### Command format: 

    ver 

#### Response format:

    ident_util <version>
    OK

#### Description: 

Print firmware version.

@subsection set `set` command (SET)  

#### Command format: 

    set <value> [<source_mask>]

#### Response format:

    OK

#### Description: 

Set state variable defined by @a source_mask .

@subsection get `get` command (GET)   

#### Command format: 

    get <sink_mask>

#### Response format:

    <value>
    OK

#### Description: 

Get state variable defined by @a sink_mask.

@}
**/
