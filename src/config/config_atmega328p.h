/** @file config_atmega328p.h
 * @brief Defines for atmega328p target device.
 */
#ifndef  CONFIG_ATMEGA328P_H
#define  CONFIG_ATMEGA328P_H

/**
@addtogroup hardware
@{
Hardware connection for Atmega328p
---------------------------------

Configuration file: @ref config_atmega328p.h

This section describes hardware connections for atmega328p based Arduino Mini Pro board.

#### Experiment Stop button ######

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PB4		| 12 (MISO)			| input, pull-up	|  abort experiment if level is low 

#### Relative encoder connections for Atmega328p ##########

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PD2/INT0| 2					| input, no pull-up	| A channel
PD3/INT1| 3					| input, no pull-up	| B channel

##### H-bridge connections (PWM output) for Atmega328p ###########

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PB1/OC1A| 9					| output			|  PWM signal, H-bridge disable 
PB2/OC1B| 10				| output			|  PWM signal, IN2: H-bridge OUT2 level is equal to IN2
PB3		| 11				| output			|  IN1, direction control: (IN1, IN2) = (high,low) --- forward, (IN1, IN2) = (low,high) --- backward 
PD7		| 7 				| output			|  enable motors, always high
PB0		| 8					| input, pull-up	|  fault signal, low level --- fault, pull-up must be enabled
PC0/ADC0| A0				| analog input		|  current feedback signal, input signal voltage should be proportional to motor current

#### Analog input connections for Atmega328p ########

10-bit ADC with reference voltage is equal to VCC.

-# Channel 1 single input:
	MCU pin		| Arduino pin		| type				| Description
	------------|-------------------|-------------------|------------
	PC1/ADC1    | A1				| analog input		| analog input

-# Channel 2 single input:
	MCU pin		| Arduino pin		| type				| Description
	------------|-------------------|-------------------|------------
	PC0/ADC0    | A0				| analog input		| analog input

@}
**/

//TIMER 3
#define TIMER_8BIT

//overflow interrupt, CTC mode
#define TIMER_SIG_TOV TIMER2_COMPA_vect
#define TIMER_TTOP OCR2A
#define TIMER_TOIE OCIE2A
//output compare match interrupts
#define TIMER_SIG_OC TIMER2_COMPB_vect
#define TIMER_OCR OCR2B
#define TIMER_OCIE OCIE2B
//registers and bits
#define TIMER_TCNT TCNT2
#define TIMER_TCCRA TCCR2A
#define TIMER_TCCRB TCCR2B
#define TIMER_CS0 CS20
#define TIMER_CS1 CS21
#define TIMER_CS2 CS22
#define TIMER_CTC WGM21
#define TIMER_TCTCR TCCR2A
#define TIMER_TIMSK TIMSK2
#define TIMER_TIFR TIFR2

//EXPERIMENT ABORT BUTTON
#define PWM_FAULT PB4
#define PWM_FAULTPORT PORTB
#define PWM_FAULTDDR DDRB
#define PWM_FAULTPIN PINB

//ENCODER
//encoder interrupt
// Encoder int handler: PD2/INT0(2) --- A channel, PD3(18) --- B channel. 
#define ENCODER_SIG_CHA_INT INT0_vect
#define ENCODER_SIG_MSKR EIMSK
#define ENCODER_SIG_IFR EIFR
#define ENCODER_SIG_IE INT0

#define ENCODER_SIG_ICR EICRA
#define ENCODER_SIG_ISC0 ISC00
#define ENCODER_SIG_ISC1 ISC01
//encoder inputs
#define ENCODER_PORT PORTD
#define ENCODER_DDR DDRD
#define ENCODER_PIN PIND
#define ENCODER_CHA PD2
#define ENCODER_CHB PD3

// PWM
// direction control
#define PWM_IN1 PB2
#define PWM_IN1PORT PORTB
#define PWM_IN1DDR DDRB
#define PWM_IN2 PB3
#define PWM_IN2PORT PORTB
#define PWM_IN2DDR DDRB
// h-bridge enable
#define PWM_EN PD7
#define PWM_ENPORT PORTD
#define PWM_ENDDR DDRD
// disable output
#define PWM_DS PB1
#define PWM_DSPORT PORTB
#define PWM_DSDDR DDRB
// h-bridge fault signal 
#define PWM_FAULT PB0
#define PWM_FAULTPORT PORTB
#define PWM_FAULTDDR DDRB
#define PWM_FAULTPIN PINB

// pwm timer
#define PWM_DS_TOCR OCR1A
#define PWM_IN2_TOCR OCR1B
#define PWM_TCCRA TCCR1A
#define PWM_TCCRB TCCR1B
#define PWM_TCCRC TCCR1C
#define PWM_TCS0 CS10
#define PWM_CS0 CS10
#define PWM_CS1 CS11
#define PWM_CS2 CS12
#define PWM_WGM0 WGM10
#define PWM_WGM1 WGM11
#define PWM_WGM2 WGM12
#define PWM_DS_COM0 COM1A0
#define PWM_DS_COM1 COM1A1
#define PWM_IN2_COM0 COM1B0
#define PWM_IN2_COM1 COM1B1

// ADC
// channel 1
#if !defined(ADC_PORT1)
// ADC0 single channel --- analog input MUX[4:0] = 0b00001
// analog reference -- AVCC: MUX[7:6] = 01 (REFS0 =1, REFS1=0)
#  define ADC_PORT1 PORTC  
#  define ADC_PIN1 PINC  
#  define ADC_DDR1 DDRC
#  define ADC_CH1 PC1
#  define ADC_DIDR1 DIDR0
#  define ADC_DID1 ADC1D
#  define ADC_ADMUX1_VALUE 0x41  
#  define ADC_ADCSRB1_MSK 0
#  define ADC_ADCSRB1_VALUE 0
#endif
// channel 2
#if !defined(ADC_PORT2)
// ADC3 single channel --- analog input ADC0: MUX[4:0] = 0b0000
// analog reference -- AVCC: MUX[7:6] = 01 (REFS0 =1, REFS1=0)
#  define ADC_PORT2 PORTC  
#  define ADC_PIN2 PINC  
#  define ADC_DDR2 DDRC
#  define ADC_CH2 PC0
#  define ADC_DIDR2 DIDR0
#  define ADC_DID2 ADC0D
#  define ADC_ADMUX2_VALUE 0x40  
#  define ADC_ADCSRB2_MSK 0
#  define ADC_ADCSRB2_VALUE 0
#endif
// control registers
#define ADC_ADMUX ADMUX
#define ADC_ADCSRA ADCSRA
#define ADC_ADCSRB ADCSRB
#define ADC_ADC ADC
#define ADC_ADCH ADCH
#define ADC_ADCL ADCL

#define ADC_REFS0 REFS0
#define ADC_REFS1 REFS1
#define ADC_ADLAR ADLAR
#define ADC_ADPS0 ADPS0
#define ADC_ADPS1 ADPS1
#define ADC_ADPS2 ADPS2
#define ADC_ADEN ADEN
#define ADC_ADIF ADIF
#define ADC_ADSC ADSC

#endif  /*CONFIG_ATMEGA328P_H*/
