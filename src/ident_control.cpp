/** 
@file ident_util.cpp
@brief Serial communication protocol implementation.

See @ref protocol .
**/

extern "C" {
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <stdio.h>

#include "uart0.h"
}

#include "sinkrealtime.hpp"
#include "sourcerealtime.hpp"
#include "regulator.hpp"
#include "regulatorcomp.hpp"

#include "parser.h"
#include "experiment.h"

const char ident_util_version[] PROGMEM =  "ident_util 2.1";

/***************************** COMMAND DEFINITION ********************************/

CMD_DEF(ibs) {
	uint16_t n, div;
	sig_mask_t mask;

	n = get_unsigned_param();
	div = get_unsigned_param();
	mask = get_optional_mask_param(source_default_mask);

	delete source;
	source = new SourceBuffered(n, div, mask);
	if (!source) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(ids) {
	if (source) source->Describe();
}

CMD_DEF(ibp) {
	// _n, _n_periods, _div
	uint16_t n, n_periods, div;
	sig_mask_t mask;

	n = get_unsigned_param();
	n_periods = get_unsigned_param();
	div = get_unsigned_param();
	mask = get_optional_mask_param(source_default_mask);

	delete source;
	source = new SourcePeriodic(n, n_periods, div, mask);
	if (!source) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(irt) {
	//  _div
	uint16_t div;
	sig_mask_t mask;
	uint16_t timeout;

	div = get_unsigned_param();
	timeout = get_unsigned_param();
	mask = get_optional_mask_param(source_default_mask);

	delete source;
	source = new SourceRealtime(div, mask, timeout);
	if (!source) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(ods) {
	if (sink) sink->Describe();
}

CMD_DEF(ops) {
	// _n, _delay, _div; 
	uint16_t n, delay, div; 
	sig_mask_t mask;

	n = get_unsigned_param();
	delay = get_unsigned_param();
	div = get_unsigned_param();
	mask = get_optional_mask_param(sink_default_mask);

	delete sink;
	sink = new SinkPrint(n, delay, div, mask);
	if (!sink) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(obs) {
	// _n, _delay, _div; 
	uint16_t n, delay, div;
	sig_mask_t mask;

	n = get_unsigned_param();
	delay = get_unsigned_param();
	div = get_unsigned_param();
	mask = get_optional_mask_param(sink_default_mask);

	delete sink;
	sink = new SinkBuffered(n, delay, div, mask);
	if (!sink) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(ort) {
	// _div; 
	uint16_t div;
	sig_mask_t mask;

	div = get_unsigned_param();
	mask = get_optional_mask_param(sink_default_mask);

	delete sink;
	sink = new SinkRealtime(div, mask);
	if (!sink) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(srt) {
	// _sample_rate
	uint16_t sample_rate;
	sample_rate = get_unsigned_param();
	set_sample_rate(sample_rate);
}

CMD_DEF(exp) {
	apply_status_t status;

	puts_P(PSTR("Start output"));

	status = experiment();

	// restore I/O config because Souce/Sink may change it
	uart0_init(); 

	switch (status) {
		case APPLY_STATUS_STOP:
			puts_P(PSTR("End output"));
			break;
		case APPLY_STATUS_ABORT:
			puts_P(PSTR("Aborted exp"));
			break;
		case APPLY_STATUS_REENTRY_ERROR:
			puts_P(PSTR("Failed exp, reentry error, control period is too small."));
			break;
		default:
			printf_P(PSTR("Failed exp, status = %d\n"), status);
			break;
	}
}

CMD_DEF(rds) {
	if (R) R->Describe();
}

CMD_DEF(rpi) {
	// PID regulator: Kp, Ki, Kd
	ParametersPID pid;
	sig_mask_t ref_mask, in_mask, out_mask;

	pid.Kp = get_sigval_param();
	pid.Ki = get_sigval_param();
	pid.Kd = get_sigval_param();
	ref_mask = get_optional_mask_param(source_default_mask);
	in_mask = get_optional_mask_param(sink_default_mask);
	out_mask = get_optional_mask_param(control_default_mask);

	delete R;
	R = new RegulatorPID(pid, in_mask, ref_mask, out_mask);
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(rrl) {
	// _K (relay  amplification coeff)
	sig_val_t Kampl;
	sig_mask_t ref_mask, in_mask, out_mask;

	Kampl = get_sigval_param();
	ref_mask = get_optional_mask_param(source_default_mask);
	in_mask = get_optional_mask_param(sink_default_mask);
	out_mask = get_optional_mask_param(control_default_mask);

	delete R;
	R = new RegulatorRelay(Kampl, in_mask, ref_mask, out_mask);
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(rtf) {
	// transfer function regulator: deg(num), deg(denum) 
	uint8_t n, m;
	sig_mask_t ref_mask, in_mask, out_mask;

	m = get_unsigned_param();
	n = get_unsigned_param();
	ref_mask = get_optional_mask_param(source_default_mask);
	in_mask = get_optional_mask_param(sink_default_mask);
	out_mask = get_optional_mask_param(control_default_mask);

	delete R;
	R = new RegulatorTF(m, n, in_mask, ref_mask, out_mask);
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(rci) {
	// thres, offset
	sig_val_t thres, offset;
	sig_mask_t ref_mask, out_mask;

	thres = get_sigval_param();
	offset = get_optional_sigval_param(0);
	ref_mask = get_optional_mask_param(source_default_mask);
	out_mask = get_optional_mask_param(control_default_mask);

	delete R;
	R = new RegulatorCompInsens(thres, offset, ref_mask, out_mask);
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(rcc) {
	// thres, offset
	sig_val_t colomb;
	sig_mask_t in_mask, out_mask, ref_mask;

	colomb = get_sigval_param();
	ref_mask = get_optional_mask_param(source_default_mask);
	in_mask = get_optional_mask_param(sink_default_mask);
	out_mask = get_optional_mask_param(control_default_mask);

	delete R;
	R = new RegulatorCompColomb(colomb, in_mask, ref_mask, out_mask);
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(rno) {
	delete R; 
	R = new RegulatorNone();
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(rol) {
	sig_mask_t  out_mask, ref_mask;

	ref_mask = get_optional_mask_param(source_default_mask);
	out_mask = get_optional_mask_param(control_default_mask);

	delete R; 
	R = new RegulatorOpenloop(ref_mask, out_mask);
	if (!R) Error::Throw(ERROR_OUT_OF_MEMORY);
}

CMD_DEF(ver) {
	puts_P(ident_util_version);
}

CMD_DEF(mod) {
	// _mode (d --- decimal IO, r --- raw 16bit x86)
	char c = *get_current_token();
	if (c == 'd') IOModeSwitcher::SetIOMode(IOModeSwitcher::DECIMAL);
	else if (c == 'r') IOModeSwitcher::SetIOMode(IOModeSwitcher::RAW);
	else Error::Throw(ERROR_ARGS);
}

CMD_DEF(get) {
	// mask
	uint16_t index;
	sig_mask_t mask;

	mask =  get_optional_mask_param(sink_default_mask);

	index = 0; mask >>= 1;
	while (mask) { index++; mask >>= 1; } 
	
	refresh_sensors();
	printf("%d\n", SIGVAL_TO_INT16(signal_buf.values[index]));
}

CMD_DEF(set) {
	// _val, _mask
	uint16_t index;
	sig_mask_t mask;
	sig_val_t val;

	val = get_sigval_param();
	mask =  get_optional_mask_param(control_default_mask);

	index = 0; mask >>= 1;
	while (mask) { index++; mask >>= 1; } 

	signal_buf.values[index] = val; 
	apply_control();
}

CMD_DEF(dex) {
	printf_P(PSTR("signals: %d 0x%x 0x%x 0x%x\n"), 8*sizeof(sig_mask_t), source_default_mask, control_default_mask, sink_default_mask);
	puts_P(experiment_description);
}

CMD_DEF(dsv) {
	uint16_t index;

	index = get_unsigned_param();
	if (index >= sizeof(sig_mask_t)*8) Error::Throw(ERROR_ARGS);
	
	puts_P((PGM_P) pgm_read_word(&experiment_signal_description[index]));
}

CMD_DEF(str) {
    uint16_t pos;
    uint16_t len;

	pos = get_unsigned_param();
    len = get_optional_unsigned_param(1);

    if (((pos+len)>experiment_user_mem_size) || len==0) 
        Error::Throw(ERROR_PARSE);
    else { 
        puts_P(PSTR("Start load"));
        SourceSerialPort::PutIOMarker(1, len);
    
        if (!SourceSerialPort::SerialPortReadSample(experiment_user_mem_ptr+pos, len)) 
            puts_P(PSTR("Failed: input error."));
        else 
            puts_P(PSTR("End load"));
    }
}

CMD_DEF(ldr) {
    uint16_t pos;
    uint16_t len;

	pos = get_unsigned_param();
    len = get_optional_unsigned_param(1);
    
    if (((pos+len)>experiment_user_mem_size) || (len==0))
        Error::Throw(ERROR_PARSE);
    else { 
        puts_P(PSTR("Start output"));
        IOModeSwitcher::PutIOMarker(1, len);
        SinkSerialPort::SerialPortWriteSample(experiment_user_mem_ptr+pos, len);
        puts_P(PSTR("End output"));
    }
}
/***************************** COMMAND DICTIONARY ********************************/


const uint8_t n_commands PROGMEM = 26;

const CommandDictionaryEntry command_dictionary[n_commands] PROGMEM = {
	CMD_CONST(dex),
	CMD_CONST(dsv),
	CMD_CONST(exp),
	CMD_CONST(get),
	CMD_CONST(ibp),
	CMD_CONST(ibs),
	CMD_CONST(ids),
	CMD_CONST(irt),
    CMD_CONST(ldr),
	CMD_CONST(mod),
	CMD_CONST(obs),
	CMD_CONST(ods),
	CMD_CONST(ops),
	CMD_CONST(ort),
	CMD_CONST(rcc),
	CMD_CONST(rci),
	CMD_CONST(rds),
	CMD_CONST(rno),
	CMD_CONST(rol),
	CMD_CONST(rpi),
	CMD_CONST(rrl),
	CMD_CONST(rtf),
	CMD_CONST(set),
	CMD_CONST(srt),
    CMD_CONST(str),
	CMD_CONST(ver)
};

int main() 
{
	uart0_init();
	stdout = &uart0;
	stdin = &uart0;
	experiment_init();
	sei();
	parse();
	return 0;
}
