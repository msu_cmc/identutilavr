#ifndef  PARSER_H
#define  PARSER_H

#include "error.h"
#include "lexer.h"

#include "signalbuf.hpp"

/*
 * @brief Macro for command execution function defifnition
 * Use \p get_XXX_param() functions to parse arguments.
 * @code
 * CMD_DEF (cmd) {
 *    int16_t p1;
 *    sig_val_t mask;
 *
 *    p1 = get_signed_param();
 *    mask = get_optional_mask_param(default_mask);
 *
 *    execute_cmd(p1, mask);
 * }
 * @endcode
 */
#define CMD_DEF(cmd) void cmd##_func()

/**
 * @brief CommandDictionaryEntry constant definition.
 */
#define CMD_CONST(cmd) { #cmd , cmd##_func }

/**
 * @brief Command dictionary entry.
 * Use macro @ref CMD_CONST for creating dictionary entry constants.
 */
struct CommandDictionaryEntry	{
	char command_str[4]; //< command string
	void (*command_func)(); //< pointer for command execution function
};

/**
 * @brief CommandDictionaryEntry array.
 * Contain CommandDictionaryEntry for each defined protocol command.
 * If you defined command with macro @ref CMD_DEF you should place corresponding CMD_CONST macro in this array.
 * Array must be sorted by @p command_str field.
 * @code
 * const CommandDictionaryEntry command_dictionary[] PROGMEM = {
 *     CMD_CONST(aba),
 *     CMD_CONST(bab),
 *     ...
 *     CMD_CONST(zap)
 *  }
 *  @endcode
 */
extern const CommandDictionaryEntry command_dictionary[] PROGMEM;
/**
 * @brief Total number of defined commands
 */
extern const uint8_t n_commands PROGMEM;
 
/**
 * @brief Parse comaands from standart input and execute them. Never return.
 */
extern void parse(void);

extern const char * get_current_token(void);

extern int16_t get_signed_param(void); 
extern uint16_t get_unsigned_param(void);
extern sig_mask_t get_optional_mask_param(void); 
extern sig_val_t get_sigval_param(void); 

extern int16_t get_optional_signed_param(int16_t def_val); 
extern uint16_t get_optional_unsigned_param(uint16_t def_val); 
extern sig_mask_t get_optional_mask_param(sig_mask_t def_val); 
extern sig_val_t get_optional_sigval_param(sig_val_t def_val); 

#endif  /*PARSER_H*/

