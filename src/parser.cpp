/** 
@file ident_util.cpp
@brief Serial communication protocol implementation.

See @ref protocol .
*/

extern "C" {
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdio.h>

#include "lexer.h"
}

#include "bool.h"
#include "error.h"
#include "signalbuf.hpp"

#include "parser.h"

static char token[LEXER_MAX_TOKEN_SIZE]; //<< current token

/** parameters parse functions */

const char * get_current_token(void)
{
	return token;
}

int16_t get_signed_param(void) 
{
	int16_t param;
	if (sscanf_P(token, PSTR("%d"), &param) != 1) Error::Throw(ERROR_PARSE);
	lexer_get_token(token);
	return param;
}

uint16_t get_unsigned_param(void) 
{
	uint16_t param;
	if (sscanf_P(token, PSTR("%u"), &param) != 1) Error::Throw(ERROR_PARSE);
	lexer_get_token(token);
	return param;
}

sig_mask_t get_mask_param(void) 
{
	uint16_t tmp;
	if (sscanf_P(token, PSTR("%x"), &tmp) != 1)  Error::Throw(ERROR_PARSE);
	lexer_get_token(token);
	return tmp;
}

sig_val_t get_sigval_param(void) 
{
	//float f;
	int16_t f;
	if (sscanf_P(token, PSTR("%d"), &f) != 1) Error::Throw(ERROR_PARSE);
	lexer_get_token(token);
	return INT16_TO_SIGVAL(f);
}

int16_t get_optional_signed_param(int16_t def_val) 
{
	int16_t param;
	if (sscanf_P(token, PSTR("%d"), &param) != 1) { param = def_val; }
	lexer_get_token(token);
	return param;
}

uint16_t get_optional_unsigned_param(uint16_t def_val) 
{
	uint16_t param;
	if (sscanf_P(token, PSTR("%u"), &param) != 1) { param = def_val; }
	lexer_get_token(token);
	return param;
}

sig_mask_t get_optional_mask_param(sig_mask_t def_val) 
{
	uint16_t tmp;
	if (sscanf_P(token, PSTR("%x"), &tmp) == 1) {
		lexer_get_token(token);
		return tmp;
	}
	else {
		lexer_get_token(token);
		return def_val;
	}
}

sig_val_t get_optional_sigval_param(sig_val_t def_val) 
{
	//float f;
	int16_t f;
	if (sscanf_P(token, PSTR("%d"), &f) == 1) {
		lexer_get_token(token);
		return INT16_TO_SIGVAL(f);
	}
	else {
		lexer_get_token(token);
		return def_val;
	}
}

void parse(void)
{
	ERROR_DEFINE
	int16_t errcode;
	void (*command_func_ptr)();
	int8_t begin, end, median;
	int16_t cmp;
	
	if (0 != (errcode = ERROR_CATCH)) {
		switch (errcode) {
			case ERROR_PARSE:
				puts_P(PSTR("Failed, protocol parse error or incorrect number of arguments."));
				break;
			case ERROR_OUT_OF_MEMORY:
				puts_P(PSTR("Failed, out of memory."));
				break;
			case ERROR_IMPLICIT_ARGS:
				puts_P(PSTR("Failed, implicit argument error."));
				break;
			case ERROR_ARGS:
				puts_P(PSTR("Failed, argument error."));
				break;
			case ERROR_IO:
				puts_P(PSTR("Failed, i/o error."));
				break;
			case ERROR_REENTRANCY:
				puts_P(PSTR("Failed, reentrancy error, sample period is to small."));
				break;
			default:
				printf_P(PSTR("Failed, errcode = %d\n"), errcode);
				break;
		}
	}

	for(;;) {
		do {
			lexer_newline();
			lexer_get_token(token);
		} while (token[0] == 0); // skip empty lines

		begin = 0; 
		end = pgm_read_word(&n_commands) - 1;
		while (begin <= end) {
			median = (begin + end) / 2;
			cmp = memcmp_P(token, command_dictionary[median].command_str, 3); 
			if ( cmp == 0 ) {
				command_func_ptr = (void (*)()) pgm_read_word(&(command_dictionary[median].command_func));
				
				lexer_get_token(token);
				(*command_func_ptr)();
			   	puts_P(PSTR("OK"));
				break;
			}
			if ( cmp > 0 ) begin = median + 1;
			else end = median - 1;
		}
		if (begin > end) {
			fputs_P(PSTR("Failed: Unknown command: "), stdout);
			puts(token);
		}
	}
}
