SET(HW_FILES ${HW_PATH}/adc.c ${HW_PATH}/encoder.c ${HW_PATH}/pwm.c ${HW_PATH}/rtrecv.c ${HW_PATH}/rtsend.c ${HW_PATH}/timer.c ${HW_PATH}/uart0.c)

ADD_EXECUTABLE(experiment_motor experiment_motor.cpp ${HW_FILES})
TARGET_LINK_LIBRARIES(experiment_motor MAIN)
TARGET_INCLUDE_DIRECTORIES(experiment_motor PRIVATE ${CMAKE_CURRENT_SOURCE_DIR} ${HW_PATH})

AVR_PROGRAM(experiment_motor)

