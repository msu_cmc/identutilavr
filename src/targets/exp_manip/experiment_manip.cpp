extern "C" {
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <string.h>
}

#include "config.h"

#include "bool.h"
#include "cpp-new.h"

extern "C" {
#include "mutex.h"
#include "timer.h"
#include "uart0.h"
#include "pwm2.h"
#include "adc.h"
}

#include "sink.hpp"
#include "source.hpp"
#include "regulator.hpp"

#include "PID_with_AWP.hpp"

#include "experiment.h"

const char experiment_description[] PROGMEM = "2 link manip: ref, pwm, position";

const char position_str1[] PROGMEM =		"position1 pulse 1 0 -32768 32767 cont sink";
const char position_str2[] PROGMEM =		"position2 pulse 1 0 -32768 32767 cont sink";
const char pwm_str1[] PROGMEM =			"pwm1 duty_cycle 9.7752e-04 0 -1023 1023 sat control";
const char pwm_str2[] PROGMEM =			"pwm2 duty_cycle 9.7752e-04 0 -1023 1023 sat control";
const char ref_str1[] PROGMEM =			"ref_position1 pulse 1 0 -32768 32767 cont source";
const char ref_str2[] PROGMEM =			"ref_position2 pulse 1 0 -32768 32767 cont source";

PGM_P const experiment_signal_description[] PROGMEM = {
	position_str1,
    position_str2,
	pwm_str1,
    pwm_str2,
	ref_str1,
    ref_str2,
	experiment_signal_none_str,
	experiment_signal_none_str
};

enum {
	POSITION1_INDEX = 0,
	POSITION2_INDEX = 1,
	PWM1_INDEX = 2,
	PWM2_INDEX = 3,
	REF1_INDEX = 4,
    REF2_INDEX = 5
};

const sig_mask_t source_default_mask = 1 << REF1_INDEX | 1 << REF2_INDEX;
const sig_mask_t sink_default_mask = 1 << POSITION1_INDEX | 1 << POSITION2_INDEX;
const sig_mask_t control_default_mask = 1 << PWM1_INDEX | 1 << PWM2_INDEX;

const uint8_t experiment_user_mem_size = 14;


sig_val_t experiment_user_mem_ptr[experiment_user_mem_size]={281,6,0,171,16,512,3840, 764,6,0,171,16,512,3840};

enum {
	KP1 = 0,
	KI1 = 1,
	KD1 = 2,
	KAWP1 = 3,
	KLIM1 = 4,
    SHIFT1 = 5,
    KFC1=6,
    KP2 = 7,
	KI2 = 8,
	KD2 = 9,
	KAWP2 = 10,
	KLIM2 = 11,
    SHIFT2 = 12,
    KFC2=13
};

static volatile mutex_t signal_buf_lock;

static volatile apply_status_t experiment_status = APPLY_STATUS_CONTINUE;

static pid_regulator pid1;
static pid_regulator pid2;

void task(void);
void task1_adc(void);
void task2_adc(void);


void apply_control(void) 
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		motor_in_pwm1(SIGVAL_TO_INT16(signal_buf.values[PWM1_INDEX]));
        motor_in_pwm2(SIGVAL_TO_INT16(signal_buf.values[PWM2_INDEX]));
	}
}

void refresh_sensors(void) 
{
    adc_select_ch1();
	adc_single_start();
	signal_buf.values[POSITION1_INDEX] = INT16_TO_SIGVAL(adc_single_wait_get());
    
    adc_select_ch2();
	adc_single_start();
	signal_buf.values[POSITION2_INDEX] = INT16_TO_SIGVAL(adc_single_wait_get());
}

void set_sample_rate(uint16_t period_mcs) 
{
	timer_set_period(period_mcs, task);
    // Start ADC1 and ADC2 earlier then main loop is called
    timer_set_oc1(period_mcs - 31*ADC_CYCLE_MCS, task1_adc);
    timer_set_oc2(period_mcs - 15*ADC_CYCLE_MCS, task2_adc);
}

void task1_adc(void)
{
    adc_select_ch1();
	adc_single_start();
}

void task2_adc(void)
{
	signal_buf.values[POSITION1_INDEX] = INT16_TO_SIGVAL(adc_single_get());
    
    adc_select_ch2();
	adc_single_start();
}


void task(void)
{
	apply_status_t status_sink, status_source;
	// lock mutex for shared variables: signal_buf, experiment_status
	if (mutex_test_set(&signal_buf_lock)) {
		experiment_status = APPLY_STATUS_REENTRY_ERROR;
		timer_stop();
		return;
	}
	sei();
    
	// update sensor's signal
	signal_buf.values[POSITION2_INDEX] = INT16_TO_SIGVAL(adc_single_get());
    
	// update reference signal
	status_source = source->Apply(signal_buf);
    
	// calculate new control
    signal_buf.values[PWM1_INDEX]=pid1.PID_with_AWP(signal_buf.values[REF1_INDEX], 
                                                            signal_buf.values[POSITION1_INDEX]);
    signal_buf.values[PWM2_INDEX]=pid2.PID_with_AWP(signal_buf.values[REF2_INDEX], 
                                                            signal_buf.values[POSITION2_INDEX]);
    
	// set control value
	apply_control();
    
	// sink output signal
	status_sink = sink->Apply(signal_buf);
    
	// operations with shared variables finished: release mutex
	mutex_release(&signal_buf_lock);

	// determine experiment status
    if (~ABORT_BUTTONPIN & _BV(ABORT_BUTTON)) { experiment_status = APPLY_STATUS_ABORT; }
	if (status_source >= APPLY_STATUS_ERROR) experiment_status = status_source;
	if (status_sink >= APPLY_STATUS_ERROR) experiment_status = status_sink;
	if (status_sink == APPLY_STATUS_STOP && status_source == APPLY_STATUS_STOP) experiment_status = APPLY_STATUS_STOP;
	if (experiment_status != APPLY_STATUS_CONTINUE) {
		timer_stop();
	}
}

void set_pid_coeff() {
    
    reg_coeff pid_coeff; 
    
    pid_coeff.kp=experiment_user_mem_ptr[KP1];
    pid_coeff.ki=experiment_user_mem_ptr[KI1];
    pid_coeff.kd=experiment_user_mem_ptr[KD1];
    pid_coeff.kawp=experiment_user_mem_ptr[KAWP1];
    pid_coeff.klim=experiment_user_mem_ptr[KLIM1];
    pid_coeff.shift=experiment_user_mem_ptr[SHIFT1];
    pid_coeff.fc=experiment_user_mem_ptr[KFC1];
    pid1.set(pid_coeff);
    
    pid_coeff.kp=experiment_user_mem_ptr[KP2];
    pid_coeff.ki=experiment_user_mem_ptr[KI2];
    pid_coeff.kd=experiment_user_mem_ptr[KD2];
    pid_coeff.kawp=experiment_user_mem_ptr[KAWP2];
    pid_coeff.klim=experiment_user_mem_ptr[KLIM2];
    pid_coeff.shift=experiment_user_mem_ptr[SHIFT2];
    pid_coeff.fc=experiment_user_mem_ptr[KFC2];
    pid2.set(pid_coeff);
}

void experiment_init(void) 
{
    motor_init_2();
	timer_init();
	
	adc_init_single();
	ABORT_BUTTONDDR &= ~_BV(ABORT_BUTTON);
	ABORT_BUTTONPORT |= _BV(ABORT_BUTTON);
    
    set_pid_coeff();
    
	R = new RegulatorNone();
}

apply_status_t experiment(void) {
    
    if (source == 0 || sink == 0 || R == 0) Error::Throw(ERROR_IMPLICIT_ARGS);
        
    source->Begin();
	sink->Begin();
    
	motor_in_pwm1(0);
    motor_in_pwm2(0);
	refresh_sensors();
   	set_pid_coeff();
	_delay_ms(2000);

	mutex_release(&signal_buf_lock);
	experiment_status = APPLY_STATUS_CONTINUE;
    
    timer_reset();
	timer_start();
    
	do { 
		sink->Poll();
		source->Poll();
	} while (experiment_status == APPLY_STATUS_CONTINUE);

    motor_in_pwm1(0);
    motor_in_pwm2(0);

    source->End();
    sink->End();
    
	return experiment_status;
}
