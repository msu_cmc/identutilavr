#ifndef PID_AWP_H
#define PID_AWP_H

typedef int16_t fixed_point16_t;
typedef int32_t fixed_point32_t;

/*coefficients for pid regulator*/
struct reg_coeff {
    fixed_point16_t kp;/*[pwm_step/enc_step*2^POINT]*/
    fixed_point16_t ki;/*[pwm_step/enc_step*2^POINT]*/
    fixed_point16_t kd;/*[pwm_step/enc_step*2^POINT]*/
    fixed_point16_t kawp;/*1/(Ts*ki);[pwm_step/enc_step*2^POINT]*/
    fixed_point16_t klim;/*u=U0*klim if limitation work */
    int16_t shift;/*shift for encoder [enc_step]*/
    fixed_point16_t fc;/*[pwm_step*2^POINT]*/
};

class pid_regulator {

    reg_coeff coeff;
    
    int16_t e_prev;/*[enc_step]*/
    fixed_point32_t sum;/*[enc_step*2^POINT]*/
    fixed_point32_t awp;/*[pwm_step*2^POINT] */
public:
    pid_regulator() {
        
        coeff.kp=0;
        coeff.ki=0;
        coeff.kd=0;
        coeff.kawp=0;
        coeff.klim=0;
        coeff.fc=0;
        e_prev=0;
        sum=0;
        awp=0;
    }
    void set(reg_coeff _coeff) {
        
        coeff=_coeff;
        e_prev=0;
        sum=0;
        awp=0;
    }
    
    //calculate pid regulator output
    int16_t PID_with_AWP(int16_t ref, int16_t sensor);
    
};

#endif
