/** @file config_atmega1280.h
 * @brief Defines for atmega1280 target device.
 */
#ifndef  CONFIG_ATMEGA1280_H
#define  CONFIG_ATMEGA1280_H

/**
@addtogroup hardware
@{
Hardware connection for Atmega1280
---------------------------------

Configuration file: @ref config_atmega1280.h

Targets: @b experiment_motor, @b experiment_servo

In this section hardware connections for atmega1280 based Arduino board is described.


#### Experiment Stop button ######

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PF1		| 36				| input, pull-up	|  abort experiment on falling edge


#### Relative encoder connections for Atmega1280 ##########

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PD2/INT2| 19				| input, no pull-up	| A channel
PD3		| 18				| input, no pull-up	| B channel

##### H-bridge connections (PWM output) for Atmega1280 ###########

MCU pin |	Arduino pin		| Type				| Description
--------|-------------------|-------------------|------------
PB5/OC1A| 11				| output			|  PWM signal, H-bridge disable 
PB6/OC1B| 12				| output			|  PWM signal, IN2: H-bridge OUT2 level is equal to IN2
PC0		| 37				| output			|  IN1, direction control: (IN1, IN2) = (high,low) --- forward, (IN1, IN2) = (low,high) --- backward 
PC3		| 34				| output			|  enable motors, always high
PC2		| 35				| input, pull-up	|  fault signal, low level --- fault, pull-up must be enabled
PF3/ADC3| A3				| analog input		|  current feedback signal, input signal voltage should be proportional to motor current

#### Analog input connections for Atmega2580 ########

10-bit ADC with reference voltage is equal to VCC.

-# Channel 1 single input:
	MCU pin		| Arduino pin		| type				| Description
	------------|-------------------|-------------------|------------
	PF0/ADC0    | A0				| analog input		| analog input

-# Channel 2 single input:
	MCU pin		| Arduino pin		| type				| Description
	------------|-------------------|-------------------|------------
	PF3/ADC3    | A3				| analog input		| analog input
	
@}
**/

//TIMER 3
#define TIMER_16BIT
//overflow interrupt, CTC mode
#define TIMER_SIG_TOV TIMER3_COMPA_vect
#define TIMER_TTOP OCR3A
#define TIMER_TOIE OCIE3A
//output compare match interrupts
//interrupt 1
#define TIMER_SIG_OC1 TIMER3_COMPB_vect
#define TIMER_OCR1 OCR3B
#define TIMER_OC1IE OCIE3B
//interrupt 2
#define TIMER_SIG_OC2 TIMER3_COMPC_vect
#define TIMER_OCR2 OCR3C
#define TIMER_OC2IE OCIE3C
//registers and bits
#define TIMER_TCNT TCNT3
#define TIMER_TCCRA TCCR3A
#define TIMER_TCCRB TCCR3B
#define TIMER_TCCRC TCCR3C
#define TIMER_CS0 CS30
#define TIMER_CS1 CS31
#define TIMER_CS2 CS32
#define TIMER_CTC WGM32
#define TIMER_TCTCR TCCR3B
#define TIMER_TIMSK TIMSK3
#define TIMER_TIFR TIFR3

//ABORT EXPERIMENT
#define ABORT_BUTTON PF5
#define ABORT_BUTTONPORT PORTF
#define ABORT_BUTTONDDR DDRF
#define ABORT_BUTTONPIN PINF

//ECODER
//encoder interrupt
// Encoder int handler: PD2/INT2(19) --- A channel, PD3(18) --- B channel. 
#define ENCODER_SIG_CHA_INT INT2_vect
#define ENCODER_SIG_MSKR EIMSK
#define ENCODER_SIG_IFR EIFR
#define ENCODER_SIG_IE INT2

#define ENCODER_SIG_ICR EICRA
#define ENCODER_SIG_ISC0 ISC20
#define ENCODER_SIG_ISC1 ISC21
//encoder inputs
#define ENCODER_PORT PORTD
#define ENCODER_DDR DDRD
#define ENCODER_PIN PIND
#define ENCODER_CHA PD2
#define ENCODER_CHB PD3

//PWM
// direction control
#define PWM_IN1 PC0
#define PWM_IN1PORT PORTC
#define PWM_IN1DDR DDRC
#define PWM_IN2 PB6
#define PWM_IN2PORT PORTB
#define PWM_IN2DDR DDRB
// h-bridge enable
#define PWM_EN PC3
#define PWM_ENPORT PORTC
#define PWM_ENDDR DDRC
// disable output
#define PWM_DS PB5
#define PWM_DSPORT PORTB
#define PWM_DSDDR DDRB
// h-bridge fault signal 
#define PWM_FAULT PC2
#define PWM_FAULTPORT PORTC
#define PWM_FAULTDDR DDRC
#define PWM_FAULTPIN PINC

// pwm timer
#define PWM_DS_TOCR OCR1A
#define PWM_IN2_TOCR OCR1B
#define PWM_TCCRA TCCR1A
#define PWM_TCCRB TCCR1B
#define PWM_TCCRC TCCR1C
#define PWM_TCS0 CS10
#define PWM_CS0 CS10
#define PWM_CS1 CS11
#define PWM_CS2 CS12
#define PWM_WGM0 WGM10
#define PWM_WGM1 WGM11
#define PWM_WGM2 WGM12
#define PWM_DS_COM0 COM1A0
#define PWM_DS_COM1 COM1A1
#define PWM_IN2_COM0 COM1B0
#define PWM_IN2_COM1 COM1B1

//ADC
// channel 1
#if !defined(ADC_PORT1)
// ADC0 single channel --- analog input MUX[5:0] = 0, MUX5 = 0
// analog reference -- AVCC: MUX[7:6] = 01 (REFS0 =1, REFS1=0)
#  define ADC_PORT1 PORTF  
#  define ADC_PIN1 PINF  
#  define ADC_DDR1 DDRF
#  define ADC_CH1 PF0
#  define ADC_DIDR1 DIDR0
#  define ADC_DID1 ADC0D
#  define ADC_ADMUX1_VALUE 0x40  
#  define ADC_ADCSRB1_MSK _BV(MUX5)
#  define ADC_ADCSRB1_VALUE 0
#endif
// channel 2
#if !defined(ADC_PORT2)
// ADC3 single channel --- analog single input ADC3: MUX[5:0] = 0b00011
// analog reference -- AVCC: MUX[7:6] = 01 (REFS0 =1, REFS1=0)
#  define ADC_PORT2 PORTF  
#  define ADC_PIN2 PINF  
#  define ADC_DDR2 DDRF
#  define ADC_CH2 PF3
#  define ADC_DIDR2 DIDR0
#  define ADC_DID2 ADC3D
#  define ADC_ADMUX2_VALUE 0x43  
#  define ADC_ADCSRB2_MSK _BV(MUX5)
#  define ADC_ADCSRB2_VALUE 0
#endif
// contril registers
#define ADC_ADMUX ADMUX
#define ADC_MUX_ADMUX_MASK 0x1f
#define ADC_MUX_ADCSRB_MASK _BV(MUX5)
#define ADC_ADCSRA ADCSRA
#define ADC_ADCSRB ADCSRB
#define ADC_ADC ADC
#define ADC_ADCH ADCH
#define ADC_ADCL ADCL

#define ADC_REFS0 REFS0
#define ADC_REFS1 REFS1
#define ADC_ADLAR ADLAR
#define ADC_ADPS0 ADPS0
#define ADC_ADPS1 ADPS1
#define ADC_ADPS2 ADPS2
#define ADC_ADEN ADEN
#define ADC_ADIF ADIF
#define ADC_ADIE ADIE
#define ADC_ADSC ADSC



#endif  /*NODE_CONFIG_ATMEGA1280_H*/
