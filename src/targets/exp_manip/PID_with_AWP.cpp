#include <stdint.h>
#include "PID_with_AWP.hpp"

/*for fixed point calculation*/
#define POINT 5

/*nominal voltage*/
/*[pwm_step*2^POINT]*/
#define U0 32736

/*max value of sensor*/
/*[enc_step]*/
#define SENSOR_MAX 1023

/*stop area for software limitation*/
/*[enc_step]*/
#define AREA 100

int16_t pid_regulator::PID_with_AWP(int16_t ref, int16_t sensor) {

    fixed_point32_t out_0;/*[pwm_step*2^POINT]*/
    int16_t e;/*[enc_step]*/
    int16_t e_d;/*[enc_step]*/
    int16_t fric_comp;/*[pwm_step]*/
    int32_t awp_term;/*[enc_step]*/

    /*calculate error and derivative error*/
    e = ref - (sensor - coeff.shift);
    e_d = e - e_prev;

    /*save error value */  
    e_prev = e;

    /*calculate integral sum with awp*/
    sum = sum + ((fixed_point32_t)e << POINT);
    awp_term=((coeff.kawp * awp) >> POINT);
    if (((sum>=0) && (awp_term>=0) && (awp_term<=sum)) || ((sum<=0) && (awp_term<=0) && (awp_term>=sum)))
        sum=sum-awp_term;
    
    /*calculate out*/
    out_0 = (fixed_point32_t)coeff.kp * e + (coeff.ki * sum >> POINT) + (fixed_point32_t)coeff.kd * e_d;

    /*calculate friction compensation*/
    if (e>0)
        fric_comp=coeff.fc;
    else if (e<0)
        fric_comp=-coeff.fc;
    else 
        fric_comp=0;
        
    /*add friction compensation*/
    out_0=out_0+fric_comp;

    /*save out before saturation and limitation */
    awp = out_0;

    /*saturation*/
    if (out_0 > U0)
        out_0 = U0;
    else if (out_0 < -U0)
        out_0 = -U0;

    /*calculate awp*/
    awp = awp - out_0;
 
    /*software motion limits*/
    if (sensor < AREA) 
        out_0 = ((fixed_point32_t)coeff.klim * U0)>>POINT;
    else if (sensor > (SENSOR_MAX - AREA)) 
        out_0 = (-(fixed_point32_t)coeff.klim * U0)>>POINT;
   
    /*write pwm */
    return (int16_t)(out_0 >> POINT);
}
