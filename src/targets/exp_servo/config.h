/** @file config.h
 * @brief Program wide defines.
 */
#ifndef  CONFIG_H
#define  CONFIG_H

#define UART0_BAUDRATE 57600
#define CLOCKRATE F_CPU

#define RT_BAUDRATE 57600
#define RTSEND_ISR
#define RTRECV_ISR

#if defined(ATMEGA1280) 
#	include "config_atmega1280.h"
#elif defined(ATMEGA2560)
#	include "config_atmega1280.h"
#elif defined(ATMEGA328P)
#	include "config_atmega328p.h"
#endif

#endif  /*CONFIG_H*/
