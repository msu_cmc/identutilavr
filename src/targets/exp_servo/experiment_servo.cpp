extern "C" {
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <string.h>
}

#include "bool.h"
#include "cpp-new.h"

extern "C" {
#include "mutex.h"
#include "timer.h"
#include "uart0.h"
#include "adc.h"
#include "pwm.h"
}

#include "sink.hpp"
#include "source.hpp"
#include "regulator.hpp"

#include "experiment.h"

const uint8_t experiment_user_mem_size=0;
sig_val_t  experiment_user_mem_ptr[experiment_user_mem_size];

const char position_str[] PROGMEM =		"position rad 0.00306796 512 0 1024 sat sink";
const char speed_str[] PROGMEM =		"speed rad/{T} 0.00306796 0 -inf +inf sat none";  
const char current_str[] PROGMEM =		"current A 0.00572917 0 -inf +inf sat none";  
const char pwm_str[] PROGMEM =			"pwm duty_cycle 9.7752e-04 0 -1023 1023 sat control";
const char ref_str[] PROGMEM =			"ref_position rad 0.00306796 512 0 1023 sat source";

const char experiment_description[] PROGMEM = "position servo: pwm input, current and position sensors";

PGM_P const experiment_signal_description[] PROGMEM = {
	position_str,
	speed_str,
	current_str,
	pwm_str,
	ref_str,
	experiment_signal_none_str,
	experiment_signal_none_str,
	experiment_signal_none_str
};

enum {
	POSITION_INDEX = 0,
	SPEED_INDEX = 1,
	CURRENT_INDEX = 2,
	PWM_INDEX = 3,
	REF_INDEX = 4
};

const sig_mask_t source_default_mask = 1 << REF_INDEX;
const sig_mask_t sink_default_mask = 1 << POSITION_INDEX;
const sig_mask_t control_default_mask = 1 << PWM_INDEX;

static volatile mutex_t signal_buf_lock;
static volatile apply_status_t experiment_status = APPLY_STATUS_CONTINUE;
static int16_t position_prev;

void task(void);
void adc2(void);
void adc1(void);

void apply_control(void) 
{
	motor_in_pwm(SIGVAL_TO_INT16(signal_buf.values[PWM_INDEX]));
}

void refresh_sensors(void) 
{
	int16_t adc_raw;	

	adc_select_ch1();
	adc_single_start();
	adc_raw = adc_single_wait_get();
	signal_buf.values[POSITION_INDEX] = INT16_TO_SIGVAL(adc_raw);
	position_prev = adc_raw;

	_delay_ms(2);

	adc_select_ch2();
	adc_single_start();
	adc_raw = adc_single_wait_get();
	signal_buf.values[CURRENT_INDEX] = INT16_TO_SIGVAL(adc_raw);
}

void set_sample_rate(uint16_t period_mcs) {
	timer_set_period(period_mcs, task);
	// Start first ADC in the middle of period  
	timer_set_oc1(period_mcs - 30*ADC_CYCLE_MCS, adc1);
	// Start second ADC 15 ADC cycles earlier then main loop is called
	timer_set_oc1(period_mcs - 15*ADC_CYCLE_MCS, adc2);
}


void experiment_init() {
	adc_init_single();
	motor_init();
	timer_init();
	// Abort button init.
	//input, pull up
	ABORT_BUTTONDDR &= ~_BV(ABORT_BUTTON);
	ABORT_BUTTONPORT |= _BV(ABORT_BUTTON);
}


void adc1(void)
{
	adc_select_ch2();
	adc_single_start();
	//// wait 2 ADC cycle before change ADC channel
	//_delay_us(2*ADC_CYCLE_MCS);
}

void adc2(void)
{
	signal_buf.values[CURRENT_INDEX] = INT16_TO_SIGVAL(adc_single_get());
	adc_select_ch1();
	adc_single_start();
}

void task(void)
{
	apply_status_t status_source, status_sink;
	
	// get servo position 
	{
		int16_t position;
		position = adc_single_get();
		signal_buf.values[POSITION_INDEX] = INT16_TO_SIGVAL(position);
		signal_buf.values[SPEED_INDEX] = INT16_TO_SIGVAL(position - position_prev);
		position_prev = position;
	}
	
	// update reference signal
	status_source = source->Apply(signal_buf);
	
	// calculate control
	if (R) R->FastControl(signal_buf);
	
	// apply control
	motor_in_pwm(SIGVAL_TO_INT16(signal_buf.values[PWM_INDEX]));	
	
	// output data
	status_sink = sink->Apply(signal_buf);

	// determine experiment status
	if (~ABORT_BUTTONPIN & _BV(ABORT_BUTTON)) { experiment_status = APPLY_STATUS_ABORT; }
	if (status_source >= APPLY_STATUS_ERROR) experiment_status = status_sink;
	if (status_sink >= APPLY_STATUS_ERROR) experiment_status = status_sink;
	if (status_sink == APPLY_STATUS_STOP && status_source == APPLY_STATUS_STOP) experiment_status = APPLY_STATUS_STOP;
	if (experiment_status != APPLY_STATUS_CONTINUE) {
		timer_stop();
	}
}

apply_status_t experiment(void) 
{
	if (source == 0 || sink == 0 || R == 0) Error::Throw(ERROR_IMPLICIT_ARGS);
	source->Begin();
	sink->Begin();

	motor_in_pwm(0);
	_delay_ms(1000);
	refresh_sensors();
	adc_select_ch2();

	mutex_release(&signal_buf_lock);
	experiment_status = APPLY_STATUS_CONTINUE;
	timer_reset();
	timer_start();
	do { 
		sink->Poll();
		source->Poll();
	} while (experiment_status == APPLY_STATUS_CONTINUE);

	motor_in_pwm(0);

	sink->End();
	source->End();
	return experiment_status;
}
