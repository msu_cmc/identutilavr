extern "C" {
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <string.h>
}

#include "bool.h"
#include "cpp-new.h"

extern "C" {
#include "timer.h"
#include "uart0.h"
#include "adc.h"
}

#include "sink.hpp"

#include "experiment.h"

const uint8_t experiment_user_mem_size=0;
sig_val_t  experiment_user_mem_ptr[experiment_user_mem_size];

const char adc0_str[] PROGMEM =		"adc0 V 0.00488759 0 0 1023 sat sink";
const char adc1_str[] PROGMEM =		"adc1 V 0.00488759 0 0 1023 sat sink";
const char adc2_str[] PROGMEM =		"adc2 V 0.00488759 0 0 1023 sat sink";
const char adc3_str[] PROGMEM =		"adc3 V 0.00488759 0 0 1023 sat sink";
const char adc8_9_str[] PROGMEM =	"adc8-adc9 V 0.00977518 0 -512 511 sat sink";
const char adc11_10_str[] PROGMEM =	"adc11-adc10 V 0.00977518 0 -512 511 sat sink";
const char voltage_str[] PROGMEM =		"voltage_ref raw 1 0 0 3 sat sink";
const char tadc_divider_str[] PROGMEM =		"tadc_divider raw 1 0 0 7 sat sink";


const char experiment_description[] PROGMEM = "osciloscope: output adc inputs";

PGM_P const experiment_signal_description[] PROGMEM = {
	adc0_str,
	adc1_str,
	adc2_str,
	adc3_str,
	adc8_9_str,
	adc11_10_str,
	voltage_str,
	tadc_divider_str,
};

enum {
	ADC0_INDEX = 0,
	ADC1_INDEX = 1,
	ADC2_INDEX = 2,
	ADC3_INDEX = 3,
	ADC8_9_INDEX = 4,
	ADC11_10_INDEX = 5,
	VOL_REF_INDEX = 6,
	TADC_DIV_INDEX = 7,
};

const uint8_t MUX_CH[] = { 0x00, 0x01, 0x02, 0x03, 0x30, 0x3b };

const sig_mask_t source_default_mask = 0;
const sig_mask_t sink_default_mask = 1 << ADC0_INDEX;
const sig_mask_t control_default_mask = 0;

static volatile apply_status_t experiment_status;
static volatile uint8_t channel_index;

void task(void);

void adc_start_conversation_series(void);
void adc_set_mode(void);
void adc_init(void);

void set_sample_rate(uint16_t period_mcs) {
	timer_set_period(period_mcs, task);
}

void experiment_init() 
{
	adc_init();
	timer_init();
	// Abort button init.
	//input, pull up
	ABORT_BUTTONDDR &= ~_BV(ABORT_BUTTON);
	ABORT_BUTTONPORT |= _BV(ABORT_BUTTON);
}

void apply_control(void) {}

void refresh_sensors(void)
{
	adc_start_conversation_series();
	while (channel_index <= ADC11_10_INDEX);
}


void task(void)
{
	apply_status_t status_sink;
	// output data
	if (channel_index <= ADC11_10_INDEX) {
		//conversation series is not finished
		experiment_status = APPLY_STATUS_REENTRY_ERROR;
	}
	status_sink = sink->Apply(signal_buf);
	adc_start_conversation_series();

	// determine experiment status
	if (~ABORT_BUTTONPIN & _BV(ABORT_BUTTON)) { experiment_status = APPLY_STATUS_ABORT; }
	if (status_sink >= APPLY_STATUS_STOP) experiment_status = status_sink;
	if (experiment_status != APPLY_STATUS_CONTINUE) {
		timer_stop();
	}
}

apply_status_t experiment(void) {
	if (sink == 0) Error::Throw(ERROR_IMPLICIT_ARGS);
	sink->Begin();

	adc_set_mode();
	_delay_ms(1000);

	timer_reset();

	experiment_status = APPLY_STATUS_CONTINUE;
	channel_index = ADC11_10_INDEX + 1;
	timer_start();
	do { 
		sink->Poll();
	} while (experiment_status == APPLY_STATUS_CONTINUE);

	sink->End();
	return experiment_status;
}


// ADC

void adc_init(void)
{
	ADC_ADMUX = 0;
	ADC_ADCSRA = 0;
	// analog reference -- AVCC 
	ADC_ADMUX |= _BV(ADC_REFS0);
	signal_buf.values[VOL_REF_INDEX] = 0x01;
	// conversation result is right aligned
	ADC_ADMUX &= ~_BV(ADC_ADLAR);
	
	// Configure ports ADC0:3 (PF0:3)
	DDRF &= ~(_BV(PF0)|_BV(PF1)|_BV(PF2)|_BV(PF3));
	PORTF &= ~(_BV(PF0)|_BV(PF1)|_BV(PF2)|_BV(PF3));
	DIDR0 |= _BV(ADC0D) | _BV(ADC1D) | _BV(ADC2D) | _BV(ADC3D);
	// Configure ports ADC8:11 (PK0:3)
	DDRK &= ~(_BV(PK0)|_BV(PK1)|_BV(PK2)|_BV(PK3));
	PORTK &= ~(_BV(PK0)|_BV(PK1)|_BV(PK2)|_BV(PK3));
	DIDR2 |= _BV(ADC8D) | _BV(ADC9D) | _BV(ADC10D) | _BV(ADC11D);
	
	// Prescaler division factor = 128, ADC_CYCLE_MCS = 128/16 = 8
	ADC_ADCSRA |= _BV(ADC_ADPS0) | _BV(ADC_ADPS1) | _BV(ADC_ADPS2); 
	signal_buf.values[TADC_DIV_INDEX] = 0x07;
	// ADC is enabled
	ADC_ADCSRA |= _BV(ADC_ADEN);
	// Interrupts is enabled.
	ADC_ADCSRA |= _BV(ADC_ADIE);
	ADC_ADCSRA |= _BV(ADC_ADIF);
	// Free running mode disabled.
}

void adc_set_mode(void)
{
	//TODO Set proper ADC ref and divider
}


void adc_start_conversation_series(void) 
{
	channel_index = ADC0_INDEX;

	ADC_ADMUX = (ADC_ADMUX & (~ADC_MUX_ADMUX_MASK)) | MUX_CH[channel_index];
	if ( MUX_CH[channel_index] & _BV(5) ) ADC_ADCSRB |= ADC_MUX_ADCSRB_MASK;
	else ADC_ADCSRB &= ~ADC_MUX_ADCSRB_MASK;
	
	// start conversation
	adc_single_start();
}

ISR(ADC_vect)
{
	int16_t tmp;
	tmp = adc_single_get();
	signal_buf.values[channel_index] = INT16_TO_SIGVAL(tmp);

	// select next channel
	channel_index++;
	if (channel_index >= ADC11_10_INDEX) {
		//conversation series ends
		return;
	}

	ADC_ADMUX = (ADC_ADMUX & (~ADC_MUX_ADMUX_MASK)) | MUX_CH[channel_index];
	if ( MUX_CH[channel_index] & _BV(5) ) ADC_ADCSRB |= ADC_MUX_ADCSRB_MASK;
	else ADC_ADCSRB &= ~ADC_MUX_ADCSRB_MASK;

	// start conversation
	adc_single_start();
}



