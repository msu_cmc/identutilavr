SET(HW_FILES ${HW_PATH}/adc.c ${HW_PATH}/rtrecv.c ${HW_PATH}/rtsend.c ${HW_PATH}/timer16bit.c ${HW_PATH}/uart0.c)

ADD_EXECUTABLE(experiment_osciloscope experiment_osciloscope.cpp ${HW_FILES})
TARGET_LINK_LIBRARIES(experiment_osciloscope MAIN)
TARGET_INCLUDE_DIRECTORIES(experiment_osciloscope PRIVATE ${CMAKE_CURRENT_SOURCE_DIR} ${HW_PATH})

AVR_PROGRAM(experiment_osciloscope)

