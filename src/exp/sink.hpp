#ifndef  SINK_HPP
#define  SINK_HPP

#include "bool.h"

#include "sourcesink.hpp"
#include "ioserial.hpp"

/******************** Sink ********************/

/**
@brief Output signal sink abstaction. Supports multidimentional signals.

All time consuming must be performed by `Set_impl()` method. `FastSet()` performs only bufferization.
*/
class Sink
{
protected:
	Sink() {} 
public:
	virtual ~Sink() {}
	virtual apply_status_t Apply(SignalBuf& signal_buf) = 0;
	virtual void Begin() = 0;
	virtual void End() = 0;
	virtual BOOL Poll() { return FALSE; }
	virtual void Describe() = 0;
};


/**
@brief Output signal sink implementation. Output signal sample is printed on each call of `Set_impl()`.

Implementation of @ref ops.
*/
class SinkPrint : public Sink, public Divider, public SinkSerialPort, public SinkVectorBase
{
protected:
	uint16_t n, cursor, wait;
	uint16_t delay;
	sig_val_t accum[8*sizeof(sig_mask_t)];
	volatile uint8_t n_accum_items_printed;
public:
	SinkPrint(uint16_t _n, uint16_t _delay, uint8_t _div, sig_mask_t _mask);

	apply_status_t Apply(SignalBuf& signal_buf);

	BOOL Poll();

	void Begin();
	void End();
	void Describe();
};

/**
@brief Output signal sink implementation. Output signal is buffered memory on call `Set_impl()` and printed only on `Flush()` call.

Implementation of @ref obs.
*/
class SinkBuffered : public SinkPrint {
protected:
	sig_val_t * buffer;
public:
	SinkBuffered(uint16_t _n, uint16_t _delay, uint8_t _div, sig_mask_t _mask);

	~SinkBuffered();

	apply_status_t Apply(SignalBuf& signal_buf);

	BOOL Poll();

	void End();
	void Describe();
};

#endif  /*SINK_HPP*/
