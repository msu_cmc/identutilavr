/** 
@file regulatorcomp.h
@brief Regulators and compensators.
*/
#ifndef  REGULATORCOMP_H
#define  REGULATORCOMP_H

#include "regulator.hpp"

/** 
@brief Compensator of insensivity zone.
*/
class RegulatorCompInsens : public RegulatorScalar
{
protected:
	sig_val_t threshold_right, threshold_left;
public:
	RegulatorCompInsens(sig_val_t _threshold, sig_val_t _offset, sig_mask_t in_mask, sig_mask_t out_mask) 
		: RegulatorScalar(in_mask, 0x1, out_mask) 
	{ 
		threshold_right = _threshold + _offset; 
		threshold_left = _threshold - _offset;
	}

	void FastControl(SignalBuf& signal_buf) 
	{
		sig_val_t in;
		in = signal_buf.values[in_index];
		if (in > 0) in += threshold_right;
		if (in < 0) in -= threshold_left;
		signal_buf.values[out_index] = in;
	}
	void Describe() 
	{
		sig_val_t threshold = (threshold_left + threshold_right) >> 1;
		sig_val_t offset = (threshold_right - threshold_left) >> 1;
		printf_P(PSTR("rci %d %d 0x%x 0x%x\n"), threshold, offset, GetSignalMask(in_index) , GetSignalMask(out_index));
	}

	~RegulatorCompInsens() {}
};

/** 
@brief Compensator of dry friction force.

Implementation of @ref rcc.
*/
class RegulatorCompColomb : public RegulatorScalar
{
protected:
	sig_val_t prev_y;
	sig_val_t colomb_comp;
public:
	RegulatorCompColomb(sig_val_t _colomb_comp, sig_mask_t in_mask, sig_mask_t ref_mask, sig_mask_t out_mask) 
		: RegulatorScalar(in_mask, ref_mask, out_mask) 
	{ 
		colomb_comp = _colomb_comp; 
		prev_y = 0;
	}

	void FastControl(SignalBuf& signal_buf) 
	{
		sig_val_t out, dy;
		
		dy = signal_buf.values[in_index] - prev_y;
		prev_y = signal_buf.values[in_index];
		out = signal_buf.values[ref_index];

		if (dy > 0 || (dy == 0 && out > 0)) {
			out += colomb_comp;
		}
		if (dy < 0 || (dy == 0 &&  out < 0)) {
			out -= colomb_comp;
		}
		signal_buf.values[out_index] = out;
	}
	void Describe() 
	{
		printf_P(PSTR("rcc %d 0x%x 0x%x 0x%x\n"), colomb_comp, GetSignalMask(ref_index), GetSignalMask(in_index), GetSignalMask(out_index));
	}

	~RegulatorCompColomb() {}
};

#endif  /*REGULATORCOMP_H*/
