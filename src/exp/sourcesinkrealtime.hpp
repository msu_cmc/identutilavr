#ifndef  SOURCESINKREALTIME_HPP
#define  SOURCESINKREALTIME_HPP

#include <avr/io.h>

class SourceSinkRealtime 
{
protected:
	//TODO Static is a bad solution if we have more then one RT interfaces.
	static uint8_t seq_number;
public:
	static uint8_t getSeqNumber() { return seq_number; }
};


#endif  /*SOURCESINKREALTIME_HPP*/
