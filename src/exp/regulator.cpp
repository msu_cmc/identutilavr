extern "C" {
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
}

#include "regulator.hpp"

/********************** RegulatorNone ***************************/

void RegulatorNone::Describe() 
{
	puts_P(PSTR("rno"));
}

/********************** RegulatorOpenloop ***********************/

void RegulatorOpenloop::Describe() 
{
	printf_P(PSTR("rol 0x%x 0x%x\n"), GetSignalMask(ref_index), GetSignalMask(out_index));
}

/********************** RegulatorRelay **************************/

void RegulatorRelay::Describe() 
{
	printf_P(PSTR("rrl %d 0x%x 0x%x 0x%x\n"), K, GetSignalMask(ref_index), GetSignalMask(in_index) , GetSignalMask(out_index));
}

/********************** RegulatorPID ****************************/

void RegulatorPID::FastControl(SignalBuf& signal_buf) 
{
	/*sig_val_t err, out;

	err = signal_buf.values[ref_index] - signal_buf.values[in_index];
	out =  pid.Kp * err;
	integral += pid.Ki * err;
	//if (integral > max_int) integral = max_int;
	//if (integral < -max_int) integral = -max_int;
	out += integral;
	out += pid.Kd * (err - err_prev);
	err_prev = err;
	signal_buf.values[out_index] = out;*/
	int32_t out;
	int16_t err;

	err = signal_buf.values[ref_index] - signal_buf.values[in_index];
	out = (int32_t) pid.Kp * err;
	integral += (int32_t) pid.Ki * err;
	out += integral >> 2;
	out += (int32_t) pid.Kd * (err - err_prev) << 2;
	err_prev = err;
	signal_buf.values[out_index] = out >> 6;
}

void RegulatorPID::Describe()
{
	printf_P(PSTR("rpi %d %d %d 0x%x 0x%x 0x%x\n"), pid.Kp, pid.Ki, pid.Kd, GetSignalMask(ref_index), GetSignalMask(in_index) , GetSignalMask(out_index));
}

/********************** RegulatorTF ****************************/

RegulatorTF::RegulatorTF(uint8_t _m, uint8_t _n, sig_mask_t _in_mask, sig_mask_t _ref_mask, sig_mask_t _out_mask)
	: RegulatorScalar(_in_mask, _ref_mask, _out_mask)
{
	BOOL error;
	uint8_t n_max;

	m = _m; 
	n = _n;
	n_max = (n > m+1) ? n : m+1;
	// memory allocation
	num = (sig_val_t *) malloc(sizeof(sig_val_t)*n_max);
	in_size = sizeof(sig_val_t)*m;
	in = (sig_val_t *) malloc(in_size);
	denum = (sig_val_t *) malloc(sizeof(sig_val_t)*n_max);
	out_size = sizeof(sig_val_t)*n;
	out = (sig_val_t *) malloc(out_size);
	if ( !num || !in || !denum || !out ) Error::Throw(ERROR_OUT_OF_MEMORY);
	// set zero inital state 
	memset(in, 0, sizeof(sig_val_t)*m);
	memset(out, 0, sizeof(sig_val_t)*n);
	out0 = 0L;
	// load regulator parameters
	puts_P(PSTR("Start load"));
	PutIOMarker(2, n_max);
	error = !SerialPortReadSample(num, n_max);
	error = error || !SerialPortReadSample(denum, n_max);
	if (error) puts_P(PSTR("Failed: input error."));
	else puts_P(PSTR("End load"));
}

void RegulatorTF::FastControl(SignalBuf& signal_buf) 
{
	int16_t err;
	err = signal_buf.values[ref_index] - signal_buf.values[in_index];
	out0 += (int32_t) num[0] * err;
	//saturation
	if ( *(((int16_t*)  &out0) + 1) >= 0x007f ) signal_buf.values[out_index] = 0x7fff;
	else if ( *(((int16_t*)  &out0) + 1) <= -0x007f ) signal_buf.values[out_index] = -0x7fff;
	else signal_buf.values[out_index] = out0 >> 8;
	if (m != 0) in[0] = err;
	if (n != 0) out[0] = signal_buf.values[out_index];
}

void RegulatorTF::Control() 
{
	out0 = 0L;
	for(uint8_t i = 0; i < m; i++) out0 += (int32_t) num[i + 1] * in[i];
	for(uint8_t i = 0; i < n; i++) out0 -= (int32_t) denum[i] * out[i];
	if (m != 0) memmove(in + 1, in, in_size - sizeof(sig_val_t));
	if (n != 0) memmove(out + 1, out, out_size - sizeof(sig_val_t));
}

void RegulatorTF::Describe()
{
	uint8_t n_max = (n > m+1) ? n : m+1;
	printf_P(PSTR("rtf %d %d 0x%x 0x%x 0x%x\n"), m, n, GetSignalMask(ref_index), GetSignalMask(in_index) , GetSignalMask(out_index));
	puts_P(PSTR("Start output"));
	PutIOMarker(1, n_max);
	SerialPortWriteSample(num, n_max);
	SerialPortWriteSample(denum, n_max);
	puts_P(PSTR("End output"));
}

RegulatorTF::~RegulatorTF()
{
	free(num);
	free(in);
	free(denum);
	free(out);
}
