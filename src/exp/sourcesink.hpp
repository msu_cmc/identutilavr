/** 
@file sourcesink.hpp
@brief Reference signal source and output signal sink abstraction.
*/
#ifndef SOURCESINK_HPP
#define SOURCESINK_HPP

#include "bool.h"
#include "error.h"
#include "signalbuf.hpp"


/**
 * @brief Exit status for semireentrancy @ref Apply method.
 * @ref Apply method is periodicaly called during experiment.
 * This method renews reference signal (source) in signal buffer or
 * copy signal from signal buffer to specified destination (sink). 
 * Apply method return variable of this data type to indicate 
 * end of experiment (e.g. there is no more samples in 
 * signal source) or error state (@ref Apply has not finished I/O 
 * operation).
 */
typedef uint8_t apply_status_t;

/**
 * @brief Exit status values.
 */
enum ApplyStatus {
	APPLY_STATUS_CONTINUE = 0, //< Continue experinent.
	APPLY_STATUS_STOP = 1, //< Stop experinent (normal exit).
	APPLY_STATUS_ERROR = 2, //< Error, abort experiment.
	APPLY_STATUS_REENTRY_ERROR = 3, //< Attempt to reentry to not reentryable code, abort experiment.
	APPLY_STATUS_ABORT = 4, //< The experiment was aborted.
};

/**
@brief Support for dividers for Source and Sink classes. 

See @ref ibs and @ref ops.
*/
class Divider {
protected:
	uint8_t div, div_step;

	Divider(uint8_t _div) 
	{ 
		div = _div; 
		div_step = 1; 
	}
	BOOL step() 
	{ 
		div_step--; 
		if (div_step == 0) { div_step = div; return 1; } 
		else return 0; 
	} 
public:
	virtual void Reset() 
	{ 
		div_step = 1; 
	}
};

class SourceSinkScalarBase
{
protected:
	uint8_t GetSignalIndex(sig_mask_t _mask) 
	{
		uint8_t sample_index = 0;
		while (_mask) { 
			if (_mask & 0x1) break;
			_mask >>= 1;
			sample_index++;
		}
		if (_mask != 0x1) Error::Throw(ERROR_ARGS);
		return sample_index;
	}
	sig_mask_t GetSignalMask(uint8_t index) 
	{
		return (sig_mask_t) 1 << index; 
	}
};

class SourceSinkVectorBase
{
protected:
	sig_mask_t mask;
	uint8_t sample_dim;

	SourceSinkVectorBase(sig_mask_t _mask) 
	{
		SetMask(_mask);
	}
public:
	void SetMask(sig_mask_t _mask) 
	{
		if (_mask == 0) Error::Throw(ERROR_ARGS);
		mask = _mask;
		sample_dim = 0;
		while (_mask) { 
			if (_mask & 0x1) sample_dim++;
			_mask >>= 1;
		}
	}
};

class SourceVectorBase : public SourceSinkVectorBase 
{
	protected:
		SourceVectorBase(sig_mask_t mask) : SourceSinkVectorBase(mask) {}

		void UnPack(const sig_val_t * src, SignalBuf& dst) 
		{
			for(uint8_t i = 0; i < 8*sizeof(sig_mask_t); i++) 
				if ((mask >> i) & 0x1) { 
					dst.values[i] =  *src;
					src++;
				}
		}
};

class SinkVectorBase : public SourceSinkVectorBase 
{
	protected:
		SinkVectorBase(sig_mask_t mask) : SourceSinkVectorBase(mask) {}

		void Pack(const SignalBuf& src, sig_val_t * dst) 
		{
			for(uint8_t i = 0; i < 8*sizeof(sig_mask_t); i++) 
				if ((mask >> i) & 0x1) { 
					*dst =  src.values[i];
					dst++;
				}
		}
};

#endif
