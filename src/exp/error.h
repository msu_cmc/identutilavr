/**
@file error.h
@brief Implementation of exceptions handling.

@ingroup error
See @ref error.
*/
#ifndef  ERROR_H
#define  ERROR_H

/**
@defgroup error Error handling
@{

AVR C++ does not support exceptions. This inplementation partly solves this problem.
This class is not thread safe.

`ERROR_DEFINE` macro defines local error variable. 
Macro ERROR_CATCH calls `setjmp` to initialize catch block and store jump point in local error variable.
If execution point leaves existence area of error variable, catch block is deinitalized.
Macro ERROR_THROW performs longjmp to the last initialized catch block which is not deinitalized yet.
So ERROR_CATCH macro must be called at least once before throwing error.

Usage example:
@code
    void function1() {
        ...
        if (error) ERROR_THROW(ERROR_CODE);
        ...
    }
    void function2() {
        ERROR_DEFINE
        ...
        if (error_code = ERROR_CATCH) {
            //error handler
            ...
        }
        ...
        function1();
        ...
    }
@endcode
*/

extern "C" {
#include    <setjmp.h>
}

/* 
@brief Error codes.
Error codes.
*/
enum ErrorCode {
	NOERROR = 0, /*!< No error, dont jump to the catch section.*/ 
	ERROR_PARSE, /*!< Communication protocol parse error. */
	ERROR_OUT_OF_MEMORY, /*!< Device does not have enought memory to perform the command.*/
	ERROR_IMPLICIT_ARGS, /*!< Required implicit arguments of the command are not set.  See @ref protocol for more information.  */
	ERROR_ARGS, /*!< Incorrect command arguments.*/
	ERROR_IO, /*!< General I/O communication error.*/
	ERROR_REENTRANCY, /*!< Second attemt to enter in not reentracy code*/
};

/** 
@brief Define error handler variable.
*/
#define ERROR_DEFINE Error _error_defined_;
/** 
@brief Initialize catch block.
Macro returns 0 if catch block is been initialized and nonzero errorcode on error.
*/
#define ERROR_CATCH ((ErrorCode) setjmp(*(_error_defined_.error_handler = &(_error_defined_.jmpb))))
/**
@brief Throw error.
@param ERRORCODE Code of error of enum ErrorCode type.
*/
#define ERROR_THROW(ERRCODE) Error::Throw(ERRCODE);

/** 
@brief Error handler variable type. 

See @ref error.
*/
class Error {
	public:
		static jmp_buf * error_handler;
		jmp_buf * prev_error_handler;
		jmp_buf jmpb;
	public:
	Error() { prev_error_handler = error_handler; }
	~Error() { error_handler = prev_error_handler; }

	/*inline ErrorCode Catch() {
		return (ErrorCode) setjmp(*(error_handler = &jmpb));
	}*/
	static void Throw(ErrorCode err);
};

/**
@}
*/

#endif  /*ERROR_H*/
