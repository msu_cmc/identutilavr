#ifndef  SOURCE_HPP
#define  SOURCE_HPP

#include "ioserial.hpp"
#include "sourcesink.hpp"

/******************** Source ********************/

/**
@brief Reference signal source abstraction.

Each instance of Source interface during creation gets @ref SignalBuf object reference.
@ref GetSample method renews content of this object.  
*/
class Source
{
public:
	virtual ~Source() {}
	virtual BOOL Apply(SignalBuf& sig_buf) = 0;
	virtual void Begin() {}
	virtual void End() {}
	virtual BOOL Poll() { return FALSE; }
	virtual void Describe() = 0;
};

/**
@brief Reference signal source implementation. Input signal is buffered in memory before experiment.

Implementation of @ref ibs.
*/
class SourceBuffered : public Source, public Divider, public SourceSerialPort, public SourceVectorBase
{
protected:
	uint16_t cursor, n;
	sig_val_t * buffer;
public:
	SourceBuffered(uint16_t _n, uint8_t _div, sig_mask_t _mask);

	~SourceBuffered();

	apply_status_t Apply(SignalBuf& signal_buf);

	void Begin();
	void Describe();
protected:
	BOOL Load();
};

/**
@brief Reference signal source implementation for periodic reference  signal. Data is buffered in memory.

Implementation of @ref ibp command.
*/
class SourcePeriodic : public SourceBuffered
{
protected:
	uint8_t n_periods, cursor_period;
public:
	SourcePeriodic(uint16_t _n, uint8_t _n_periods, uint8_t _div, sig_mask_t _mask);

	BOOL Apply(SignalBuf& signal_buf);

	void Begin();
	void Describe(); 
};

#endif  /*SOURCE_HPP*/
