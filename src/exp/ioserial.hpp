#ifndef  IOSERIAL_HPP
#define  IOSERIAL_HPP

#include "bool.h"
#include "signalbuf.hpp"


/**
@brief Support for changing I/O mode. 

Allow to switch beetween `raw` and `decimal` I/O mode.
See @ref mod.
*/
class IOModeSwitcher 
{
public: 
	enum IOMode { DECIMAL = 0, RAW = 1 };
protected:
	static IOMode io_mode;

	IOModeSwitcher() {}
public:
	static void SetIOMode(IOMode _mode) 
	{ 
		io_mode = _mode; 
	}
	static IOMode GetIOMode() 
	{ 
		return io_mode; 
	}
	static void PutIOMarker(uint16_t n, uint8_t sample_dim);
};


class SourceSerialPort : public IOModeSwitcher
{
public:
	static BOOL SerialPortReadSample(sig_val_t * dst, uint8_t sample_dim);
};

class SinkSerialPort : public IOModeSwitcher
{
public:
	static BOOL SerialPortWrite(const sig_val_t& src, BOOL end_sample); 
	static BOOL SerialPortWriteSample(sig_val_t * src, uint8_t sample_dim);
};


#endif  /*IOSERIAL_HPP*/
