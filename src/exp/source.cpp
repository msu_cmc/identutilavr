extern "C" {
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdio.h>
}

#include "bool.h"
#include "error.h"

#include "source.hpp"

/////////////////// SourceBuffered //////////////////////

SourceBuffered::SourceBuffered(uint16_t _n, uint8_t _div, sig_mask_t _mask) 
	: Divider(_div), SourceVectorBase(_mask)
{
	n = _n*sample_dim; // buffer length
	cursor = 0;
	buffer = (sig_val_t *) malloc(sizeof(sig_val_t) * n);
	if (buffer) {
		if (Load()) Error::Throw(ERROR_IO);
	}
	else Error::Throw(ERROR_OUT_OF_MEMORY);
}	

SourceBuffered::~SourceBuffered()
{
	free(buffer);
}

apply_status_t SourceBuffered::Apply(SignalBuf& signal_buf) 
{
	if (cursor == n) return  APPLY_STATUS_STOP; 
	if (step()) {
		UnPack(buffer + cursor, signal_buf);
		cursor += sample_dim;
	}
	return APPLY_STATUS_CONTINUE;
}

void SourceBuffered::Begin()
{
	Divider::Reset();
	cursor = 0;
}

void SourceBuffered::Describe() {
	printf_P(PSTR("ibs %d %d 0x%x\n"), n, div, mask);
}

BOOL SourceBuffered::Load()
{
	BOOL error = 0;
	Begin();

	puts_P(PSTR("Start load"));
	PutIOMarker(n / sample_dim, sample_dim);

	for(cursor = 0; cursor < n; cursor += sample_dim) {
		error = error || !SerialPortReadSample(buffer + cursor, sample_dim);
	}
	if (error) puts_P(PSTR("Failed: input error."));
	else puts_P(PSTR("End load"));

	return error;
}

/////////////////// SourcePeriodic //////////////////////

SourcePeriodic::SourcePeriodic(uint16_t _n, uint8_t _n_periods, uint8_t _div, sig_mask_t _mask)
	: SourceBuffered(_n, _div, _mask)
{
   n_periods = _n_periods; 
   cursor_period = 0; 
}

apply_status_t SourcePeriodic::Apply(SignalBuf& signal_buf)
{
	if (cursor == n) {
		if (cursor_period == n_periods - 1) return  APPLY_STATUS_STOP;
		else {
			cursor_period++;
			cursor = 0;
		}
	}
	if (step()) {
		UnPack(buffer + cursor, signal_buf);
		cursor += sample_dim;
	}
	return APPLY_STATUS_CONTINUE;
}

void SourcePeriodic::Begin() 
{
	SourceBuffered::Begin();
	cursor_period = 0;
} 

void SourcePeriodic::Describe() {
	printf_P(PSTR("ibp %d %d %d 0x%x\n"), n, n_periods, div, mask);
}
