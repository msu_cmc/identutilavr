extern "C" {
#	include <avr/pgmspace.h>
#	include <stdio.h>

#	include "rtsend.h"
}

#include "sink.hpp"
#include "sourcesinkrealtime.hpp"

class SinkRealtime : public Sink, public Divider, public SinkVectorBase, public SourceSinkRealtime
{
public:
	SinkRealtime(uint8_t _div, sig_mask_t _mask) 
		: Divider(_div), SinkVectorBase(_mask)
	{
	}	

	~SinkRealtime()
	{
	}

	apply_status_t Apply(SignalBuf& signal_buf) 
	{
		uint8_t * accum;
		if (step()) {
			accum = (uint8_t *) rtsend_get_accum_lock();
			accum[0] = seq_number;
			Pack(signal_buf, (sig_val_t *) (accum + 1));
			rtsend_accum_release();
			//printf("O %d %d\n", rtsend_get_state(), *((sig_val_t *) (accum + 1)));
		}
		return APPLY_STATUS_STOP;
	}

	void Begin()
	{
		Divider::Reset();

		rtsend_set_mode(RTSEND_STOP);
#if !defined(RTSEND_ISR)
		rtsend_poll();
#endif
		if (! rtsend_init(1 + sample_dim * sizeof(sig_val_t))) Error::Throw(ERROR_OUT_OF_MEMORY);
		rtsend_set_mode(RTSEND_SEND_WAIT_ACCUM);
	}

	BOOL Poll()
	{
#if !defined(RTSEND_ISR)
		rtsend_poll();
#endif
		return TRUE;
	}

	void End() {
		rtsend_set_mode(RTSEND_STOP);
#if !defined(RTSEND_ISR)
		rtsend_poll();
#endif
		rtsend_deinit();	
	}

	void Describe() {
		printf_P(PSTR("ort %d 0x%x\n"), div, mask);
	}
};
