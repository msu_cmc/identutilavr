/** 
@file cpp-new.h
@brief Implementation of `new` and `delete` C++ operators.
*/
#ifndef CPPNEW_H
#define CPPNEW_H

void * operator new(size_t size);
void operator delete(void * ptr); 
void operator delete[](void * ptr); 

extern "C" void __cxa_pure_virtual(void); 

#endif
