extern "C" {
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdio.h>
}

#include "bool.h"
#include "ioserial.hpp"

/////////////////// IOModeSwitcher //////////////////////

IOModeSwitcher::IOMode IOModeSwitcher::io_mode = IOModeSwitcher::DECIMAL;

void IOModeSwitcher::PutIOMarker(uint16_t n, uint8_t sample_dim) 
{
	switch (io_mode) {
		case DECIMAL:
			printf_P(PSTR("decimal %d %d\n"), n, sample_dim);
			break;
		case RAW:
			printf_P(PSTR(SIGVAL_IO_MARKER" %d %d\n"), n, sample_dim);
			break;
	}
}

/////////////////// SourceSerialPort //////////////////////

BOOL SourceSerialPort::SerialPortReadSample(sig_val_t * dst, uint8_t sample_dim)
{
	switch (io_mode) {
		case DECIMAL:
			for(uint8_t i = 0; i < sample_dim; i++) {
				if (scanf("%d", dst + i) != 1) return FALSE;
			}
			return TRUE;
		case RAW:
				if (fread(dst, sizeof(sig_val_t), sample_dim, stdin) != sample_dim) return FALSE;
			return TRUE;	
	}
	return FALSE;
}

/******************** SinkSerialPort ********************/

BOOL SinkSerialPort::SerialPortWrite(const sig_val_t& src, BOOL end_sample) 
{
	switch (io_mode) {
		case DECIMAL:
			printf("%d ", src);
			if (end_sample) putc('\n', stdout);
			break;
		case RAW:
			fwrite(&src, sizeof(sig_val_t), 1, stdout);
			break;
	}
	return TRUE;
}

BOOL SinkSerialPort::SerialPortWriteSample(sig_val_t * src, uint8_t sample_dim)
{
	switch (io_mode) {
		case DECIMAL:
			for(uint8_t i = 0; i < sample_dim; i++) printf("%d ", src[i]);
			putc('\n', stdout);
			break;
		case RAW:
			fwrite(src, sizeof(sig_val_t), sample_dim, stdout);
			break;
	}
	return TRUE;
}

