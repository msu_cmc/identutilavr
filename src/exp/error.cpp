#include "error.h"
 // #include "stdio.h"

jmp_buf *  Error::error_handler = 0;

void Error::Throw(ErrorCode err) {
	if (! error_handler) {
		// puts("unhandled exception");
		for(;;) {}
	}
	// puts("trow err");
	longjmp(*error_handler, (int) err);
}
