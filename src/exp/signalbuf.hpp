#ifndef  SIGNALBUF_HPP
#define  SIGNALBUF_HPP

extern "C" {
#include <avr/io.h>
#include <stdfix.h>
}

typedef int16_t sig_val_t; /**< Signal data type. */

typedef uint8_t sig_mask_t; /**< Signal buffer mask data type. */

#define SIGVAL_IO_MARKER "int16x86" /**< IO marker for signal data type. */

/**
 * Sigal data type conversation macros. 
 * @{
 */
#define RAW_TO_SIGVAL(X) (*((const sig_val_t *) &(X))) 
#define INT16_TO_SIGVAL(X) ((sig_val_t) (X))
#define UINT16_TO_SIGVAL(X) ((sig_val_t) (X))
#define FLOAT_TO_SIGVAL(X) ((sig_val_t) (X))
 
#define SIGVAL_TO_RAW(X) (*((const uint16_t *) &(X)))
#define SIGVAL_TO_INT16(X) ((int16_t) (X))
#define SIGVAL_TO_UINT16(X) ((uint16_t) (X))
#define SIGVAL_TO_FLOAT(X) ((float) (X))
/**
 * @}
 */

/**
 * @brief Signal buffer (state vector) data type.
 * Data type to store all signals in target device.
 **/
struct SignalBuf
{
	sig_val_t values[8*sizeof(sig_mask_t)]; /**< Signal values. */
};

#endif  /*SIGNALBUF_H*/
