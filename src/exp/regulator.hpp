/** 
@file regulator.h
@brief Interface of regulator implementation.
*/
#ifndef REGULATOR_H
#define REGULATOR_H

#include "signalbuf.hpp"
#include "sourcesink.hpp"
#include "ioserial.hpp"

/**
@brief Regulator abstraction.
*/
class Regulator 
{
public:
	virtual void FastControl(SignalBuf& signal_buf) = 0;
	virtual void Control() {}
	virtual void Describe() = 0;
	virtual ~Regulator() {}
};

class RegulatorScalar : public Regulator, public SourceSinkScalarBase 
{
protected:
	uint8_t in_index;
	uint8_t ref_index;
	uint8_t out_index;
protected:
	RegulatorScalar(sig_mask_t in_mask, sig_mask_t ref_mask, sig_mask_t out_mask) 
	{
		in_index = GetSignalIndex(in_mask);
		ref_index = GetSignalIndex(ref_mask);
		out_index = GetSignalIndex(out_mask);
	}
};

class RegulatorNone : public Regulator
{
public:
	RegulatorNone() {}
	void FastControl(SignalBuf& signal_buf) {}
	void Describe();
};

/** 
@brief Dummy regulator: control = reference.
*/
class RegulatorOpenloop : public RegulatorScalar
{
public:
	RegulatorOpenloop(sig_mask_t ref_mask, sig_mask_t out_mask)
		: RegulatorScalar(0x1, ref_mask, out_mask) {} 

	void FastControl(SignalBuf& signal_buf) 
	{ 
		signal_buf.values[out_index] = signal_buf.values[ref_index];
	}
	void Describe();
};

/**
@brief PID coefficients structure.
*/
struct ParametersPID {
	sig_val_t Kp, Ki, Kd;
	sig_val_t N;
};

/**
@brief Digital PID regulator.

Implementation of @ref rpi.
*/
class RegulatorPID : public RegulatorScalar {
protected:
	ParametersPID pid;
	//sig_val_t integral;
	int32_t integral;
	sig_val_t err_prev;
public:
	RegulatorPID(const ParametersPID& _pid, sig_mask_t in_mask, sig_mask_t ref_mask, sig_mask_t out_mask) 
		: RegulatorScalar(in_mask, ref_mask, out_mask) 
	{ 
		pid = _pid;
		integral = 0; err_prev = 0;
	}

	void SetParameters(const ParametersPID& _pid) 
	{
		pid = _pid;
	}
	void FastControl(SignalBuf& signal_buf);
	void Describe();
};

/**
@brief Relay regulator.

Implementation of @ref rrl.
*/
class RegulatorRelay : public RegulatorScalar
{
protected:
	sig_val_t K;
public:
	RegulatorRelay(sig_val_t _K, sig_mask_t in_mask, sig_mask_t ref_mask, sig_mask_t out_mask) 
		: RegulatorScalar(in_mask, ref_mask, out_mask) 
	{ 
		K = _K; 
	}

	void FastControl(SignalBuf& signal_buf) 
	{
		sig_val_t out;
		out = signal_buf.values[ref_index] - signal_buf.values[in_index];
		if (out > 0) out = K;
		else if (out < 0) out = - K;
		else out = 0;
		signal_buf.values[out_index] = out;
	}
	void Describe();
};

/**
@brief Relay regulator.

Implementation of @ref rtf.
*/
class RegulatorTF : public RegulatorScalar, public SourceSerialPort, public SinkSerialPort
{
protected:
	uint8_t m, n, in_size, out_size;
	sig_val_t * num, * denum;
	sig_val_t * in, * out;
	//int32_t * out;
	int32_t out0; 
public:
	RegulatorTF(uint8_t _m, uint8_t _n, sig_mask_t in_mask, sig_mask_t ref_mask, sig_mask_t out_mask);

	void FastControl(SignalBuf& signal_buf);
	void Control();
	void Describe();

	~RegulatorTF();
};

#endif
