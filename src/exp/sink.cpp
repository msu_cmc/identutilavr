extern "C" {
//#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdlib.h>
}

#include "bool.h"
#include "error.h"

#include "sink.hpp"

/******************** SinkPrint ********************/

SinkPrint::SinkPrint(uint16_t _n, uint16_t _delay, uint8_t _div, sig_mask_t _mask) 
	:  Divider(_div), SinkVectorBase(_mask)
{ 
   n = _n*sample_dim; 
   delay = _delay;
   wait = delay; 
   cursor = 0; 
   n_accum_items_printed = 0;
}

apply_status_t SinkPrint::Apply(SignalBuf& signal_buf) 
{
	if (wait) { 
		wait--; 
		return APPLY_STATUS_CONTINUE; 
	}
	if (cursor == n) return  APPLY_STATUS_STOP;
	if (step()) {
		// check if output operation finished: n_accum_items_printed is atomic, ATOMIC_BLOCK not needed.
		if (n_accum_items_printed < sample_dim) return APPLY_STATUS_REENTRY_ERROR;
		Pack(signal_buf, accum);
		//SerialPortWriteSample(accum);
		n_accum_items_printed = 0;
		cursor += sample_dim;
	}
	return APPLY_STATUS_CONTINUE;
}

void SinkPrint::Begin() 
{
	Divider::Reset();
	cursor = 0;
	wait = delay;
	n_accum_items_printed = sample_dim;
	PutIOMarker(n / sample_dim, sample_dim);
}

BOOL SinkPrint::Poll() {
	// n_accum_items_printed is atomic
	if (n_accum_items_printed == sample_dim) return FALSE;
	SerialPortWrite(accum[n_accum_items_printed], n_accum_items_printed == sample_dim - 1);
	//only this function can change this varable if n_accum_items_printed < sample_dim
	n_accum_items_printed++;
	return TRUE;
}

void SinkPrint::End() {
	while (n_accum_items_printed < sample_dim) {
		SerialPortWrite(accum[n_accum_items_printed], n_accum_items_printed == sample_dim - 1);
		n_accum_items_printed++;
	}
	while (cursor < n) {
		SerialPortWriteSample(accum, sample_dim);
		cursor += sample_dim;
	}
}

void SinkPrint::Describe()
{
	printf_P(PSTR("ops %d %d %d 0x%x\n"), n, delay, div, mask);
};

/******************** SinkBuffered ********************/

SinkBuffered::SinkBuffered(uint16_t _n, uint16_t _delay, uint8_t _div, sig_mask_t _mask)
	:  SinkPrint(_n, _delay, _div, _mask)
{
	buffer = (sig_val_t *) malloc(sizeof(sig_val_t)*n);
	if (!buffer) Error::Throw(ERROR_OUT_OF_MEMORY);
}

SinkBuffered::~SinkBuffered() 
{
	free(buffer);
}

apply_status_t SinkBuffered::Apply(SignalBuf& signal_buf)
{
	if (wait) { 
		wait--; 
		return APPLY_STATUS_CONTINUE; 
	}
	if (cursor == n) return  APPLY_STATUS_STOP;
	if (step()) {
		Pack(signal_buf, buffer + cursor);
		cursor += sample_dim;
	}
	return APPLY_STATUS_CONTINUE;
}

BOOL SinkBuffered::Poll() { return FALSE; }

void SinkBuffered::End()
{
	for(uint16_t i = 0; i < n; i += sample_dim) {
		SerialPortWriteSample(buffer + i, sample_dim);
	}
}

void SinkBuffered::Describe() {
	printf_P(PSTR("obs %d %d %d 0x%x\n"), n, delay, div, mask);
}
