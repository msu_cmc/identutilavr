#ifndef SOURCEREALTIME_H
#define SOURCEREALTIME_H

extern "C" {
#	include <avr/pgmspace.h>
#	include <stdio.h>

#	include "rtrecv.h"
}

#include "source.hpp"
#include "sourcesinkrealtime.hpp"

class SourceRealtime : public Source, public Divider, public SourceVectorBase, public SourceSinkRealtime
{
protected:
	uint8_t timeout;
	uint8_t missing_frame_counter;
public:
	SourceRealtime(uint8_t _div, sig_mask_t _mask, uint8_t _timeout) 
		: Divider(_div), SourceVectorBase(_mask), timeout(_timeout)
	{}	

	~SourceRealtime()
	{}

	apply_status_t Apply(SignalBuf& signal_buf) 
	{
		uint8_t * accum;
		// receive data
		if (step()) {
			accum =  (uint8_t *) rtrecv_get_accum_lock();
			if (!rtrecv_is_accum_updated()) missing_frame_counter++;
			else missing_frame_counter = 0;
			seq_number = *accum;
			UnPack((sig_val_t *) (accum + 1), signal_buf);
			rtrecv_accum_release();
			//printf("I %d %d\n", rtrecv_get_state(), *((sig_val_t *) (accum + 1)));
		}
		// check if timeout is hit
		if (timeout == 0 || missing_frame_counter < timeout) return APPLY_STATUS_CONTINUE;
		else return APPLY_STATUS_STOP;
	}

	void Begin()
	{
		Divider::Reset();

		rtrecv_set_mode(RTRECV_STOP);
#if !defined(RTRECV_ISR)
		rtrecv_poll();
#endif
		//buffer length + seq1 byte
		if (! rtrecv_init(1 + sample_dim * sizeof(sig_val_t))) Error::Throw(ERROR_OUT_OF_MEMORY);
		rtrecv_set_mode(RTRECV_RECV_CONT);

		missing_frame_counter = 0;
	}

	BOOL Poll()
	{
#if !defined(RTRECV_ISR)
		rtrecv_poll();
#endif
		return TRUE;
	}

	void End() {
		rtrecv_set_mode(RTRECV_STOP);
#if !defined(RTRECV_ISR)
		rtrecv_poll();
#endif
		rtrecv_deinit();	
	}

	void Describe() {
		printf_P(PSTR("irt %d 0x%x\n"), div, mask);
	}
};

#endif
